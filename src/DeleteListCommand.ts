import { QuantrCommand } from './QuantrCommand';
import * as spauth from 'node-sp-auth';
import * as request from 'request-promise';
import { getAuth, createall, gettop, ensureuser, deleteList } from './function';
const Json2csvParser = require('json2csv').Parser;
const fs = require('fs');
const chalk = require('chalk');
//delete all list in site
export class DeleteListCommand extends QuantrCommand {
    public color = chalk.rgb(255, 255, 0);
	
	public run() {
        super.log("Delete List");
        var info = {
			fromUsername: this.fromUsername,
			fromPassword: this.fromPassword,
			fromTenant: this.fromTenant,
			fromSite: this.fromSite,
			fromList: this.fromList,
		}

        spauth.getAuth('https://' + this.fromTenant + '.sharepoint.com',{
            username: this.fromUsername,
            password: this.fromPassword
        }).then(data => {
            var headers = data.headers;
            headers['Accept'] = 'application/json; odata=verbose';
            request.get({
                url: "https://" + this.fromTenant + ".sharepoint.com" + this.fromSite + "/_api/web/lists",
                headers: headers,
                json: true
            }).then(async function (listresponse){
                //console.log(listresponse.d.results.length);
                for (var i = 0; i < listresponse.d.results.length; i++) {
                    if(listresponse.d.results[i].BaseTemplate == 100){
                        //delete list by title
                        
                    }                    
                    console.log(listresponse.d.results[i].BaseTemplate+"    :   "+listresponse.d.results[i].Title);
                }
            })
        })
    }

    public help() {

    }
}