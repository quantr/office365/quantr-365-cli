import { QuantrCommand } from './QuantrCommand';
import * as spauth from 'node-sp-auth';
import * as request from 'request-promise';
const Json2csvParser = require('json2csv').Parser;
const fs = require('fs');
const chalk = require('chalk');

export class ExportListCommand extends QuantrCommand {
	public color = chalk.rgb(255, 255, 0);

	public run() {
		super.log("Export List");  
		let title = this.fromList; 
        //console.log("from site is:"+this.fromSite);
        //console.log("https://"+this.fromTenant+".sharepoint.com"+this.fromSite+"/_api/web/lists/getByTitle('"+this.fromList+"')/items/?$select=*");
		spauth.getAuth('https://' + this.fromTenant + '.sharepoint.com', {
			username: this.fromUsername,
			password: this.fromPassword
		}).then(data => {
			var headers = data.headers;
			headers['Accept'] = 'application/json;odata=verbose';
            
			request.get({
				url: "https://"+this.fromTenant+".sharepoint.com"+this.fromSite+"/_api/web/lists/getByTitle('"+this.fromList+"')/items/?$select=*",
				headers: headers,
				json: true
			}).then(function(listresponse){
                var responseJSON = listresponse.d.results;
                const json2csvParser = new Json2csvParser({header:true});
                const csv = json2csvParser.parse(JSON.parse(JSON.stringify(responseJSON, null, 4)));

                fs.writeFile(title+'.csv',csv,function(error: any){
                    if(error) throw error;
                });
			});
		});
	}

	public help() {

	}
}