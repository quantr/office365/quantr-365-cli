import { QuantrCommand } from './QuantrCommand';
import * as spauth from 'node-sp-auth';
import * as request from 'request-promise';
import { getAuth, createall, gettop, ensureuser } from './function';
const Json2csvParser = require('json2csv').Parser;
const fs = require('fs');
const chalk = require('chalk');

export class DownloadFileCommand extends QuantrCommand {
	public color = chalk.rgb(255, 255, 0);

	public run() {
		super.log("Download File");
		let that = this;
		spauth.getAuth('https://' + this.fromTenant + '.sharepoint.com', {
			username: this.fromUsername,
			password: this.fromPassword
		}).then(data => {
			var headers = data.headers;

			var fs = require('fs');
			var request2 = require('request');
			var progress = require('request-progress');

			// The options argument is optional so you can omit it 
			progress(request2({
				url: this.url,
				headers: headers
			}), {
				// throttle: 2000,                    // Throttle the progress event to 2000ms, defaults to 1000ms 
				// delay: 1000,                       // Only start to emit after 1000ms delay, defaults to 0ms 
				// lengthHeader: 'x-transfer-length'  // Length header to use, defaults to content-length 
			}).on('progress', function (state: any) {
				// The state is an object that looks like this: 
				// { 
				//     percent: 0.5,               // Overall percent (between 0 to 1) 
				//     speed: 554732,              // The download speed in bytes/sec 
				//     size: { 
				//         total: 90044871,        // The total payload size in bytes 
				//         transferred: 27610959   // The transferred payload size in bytes 
				//     }, 
				//     time: { 
				//         elapsed: 36.235,        // The total elapsed seconds since the start (3 decimals) 
				//         remaining: 81.403       // The remaining seconds to finish (3 decimals) 
				//     } 
				// } 
				console.log('progress', state);
			})
				.on('error', function (err: any) {
					// Do something with err 
				})
				.on('end', function () {
					// Do something after request finishes 
				})
				.pipe(fs.createWriteStream('a.iso'));
		});
	}

	public help() {

	}
}