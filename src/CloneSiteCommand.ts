import { QuantrCommand } from './QuantrCommand';
import './function'
const request = require ('request-promise')//.defaults({ family: 4 });
const req = require ('request')
const progress = require('request-progress');
//import * as request from 'request-promise';
import { getAuth, createall, gettop, createlog, clonelist, getitem, itemcount,createDocLib, copyFolder, copyFile, getdocumentlib, getColumnTitle, getColumn, uploadLargeFile, guid_genearator, createFileURL, getFileSize, updateColumn, decodeName } from './function';
const Json2csvParser = require('json2csv').Parser;
const fs = require('fs');
const chalk = require('chalk');
const clipg = require('cli-progress')

export class CloneSiteCommand extends QuantrCommand {
    public color = chalk.rgb(255,255,0)
    
    public run(){
        var info = {
            fromUsername:this.fromUsername,
            fromPassword:this.fromPassword,
            fromTenant:this.fromTenant,
            fromSite:this.fromSite,
            toTenant:this.toTenant,
            toSite:this.toSite,
            toUsername:this.toUsername,
            toPassword:this.toPassword
        }
        if(this.toUsername!=null || this.toUsername!=undefined){
			info['toUsername'] = this.toUsername;
			info['toPassword'] = this.toPassword;
			Dest = true;
		}else{
			info['toUsername'] = this.fromUsername;
			info['toPassword'] = this.fromPassword;
			Dest = false;
        }

        var Dest: boolean;
        
        var auth:any;
        var SiteList = new Map();
        var DocLib = new Map();
        var topList = new Map();
        var userfieldMap = new Map();
        var renameMap = new Map();
        // var useremailMap = new Map();
        // var destemailMap = new Map();
        var combineMap = new Map ();
        var DocMap = new Map(); 
        var barsloop: number = 0;
        var errorLog = '';
        var tempFile:any;

        const bars: any[]=[];

		const multibar = new clipg.MultiBar({
			format: '{progress} |' + chalk.rgb(204,0,204)('{bar}') + '| {percentage}% || {value}/{total} Chunks',
			barCompleteChar: '\u2588',
			barIncompleteChar: '\u2591',
			hideCursor: true,
            stopOnComplete: true,
		});


        (async()=>{
            auth = await getAuth(info);
            //await getLargeFile(info,auth);
            //await uploadLargeFile(info,auth,'Test','math1014.pdf')


            await getlistinsite(); 
            
            topList = await gettop(info,auth,SiteList)

            var list = SiteList.keys()

            if(this.itemindex!=undefined && this.listindex!=undefined){
				console.log("Continue cloning ......")
                var newlist = SiteList.keys()
                for(var i = 0; i < this.listindex; i++){
					SiteList.delete(newlist.next().value)
				}
				var currentlist = list.next().value
				
				bars.push(multibar.create(topList.get(currentlist),0,{progress:currentlist + " items"}));
				barsloop++;
				bars[barsloop-1].update(this.itemindex-1)

				var itemloop = (topList.get(currentlist)-this.itemindex)/1000
				for(var j = 0; j < itemloop; j++){
					await getitem(auth,info,currentlist,(this.itemindex+(1000*j)),((this.itemindex+1000*j)+999),this.listindex,userfieldMap,combineMap,renameMap,topList,bars,barsloop,multibar)
					//console.log((this.itemindex+(1000*j))+"----->"+((this.itemindex+1000*j)+999))
				}
				SiteList.delete(currentlist)
				
				for(var loopi = 0; loopi < SiteList.size; loopi++){
					var listname = list.next().value
					bars.push(multibar.create(topList.get(listname),0,{progress:listname + " items"}));
					barsloop++;
					if(topList.get(listname)>1000){
						var loop = topList.get(listname)/1000

						for(var i = 0; i < loop; i++){
							await getitem(auth,info,listname,((1000*i)+1),(1000*(i+1)),loopi,userfieldMap,combineMap,renameMap,topList,bars,barsloop,multibar)
						}
					}else{
						await getitem(auth,info,listname,1,topList.get(listname),loopi,userfieldMap,combineMap,renameMap,topList,bars,barsloop,multibar)
					}
				}
            }else if(this.itemindex==undefined && this.listindex!=undefined){
                console.error(chalk.bgRed.bold("Missing ItemIndex!"))
            }else if(this.itemindex!=undefined && this.listindex==undefined){
                console.error(chalk.bgRed.bold("Missing ListIndex!"))
            }
            else{
                let temp = await createlog(Dest,info,auth,SiteList,bars,multibar,barsloop,topList)

                barsloop = temp.barsloop
                userfieldMap = temp.userfieldMap
                combineMap = temp.combineMap

                var listID = await createall(info,auth,SiteList)
				var min=1; //default
                let clone = await clonelist(auth,info,SiteList,listID,bars,multibar,barsloop)
                renameMap = clone.renameMap
                barsloop = clone.barsloop
				var list = SiteList.keys()
				for(var loopi = 0; loopi < SiteList.size; loopi++){
					var listname = list.next().value
					bars.push(multibar.create(topList.get(listname),0,{progress:listname + " items"}));
                    barsloop++;
					if(topList.get(listname)>1000){
						var loop = topList.get(listname)/1000

						for(var i = 0; i < loop; i++){
							await getitem(auth,info,listname,((1000*i)+1),(1000*(i+1)),loopi,userfieldMap,combineMap,renameMap,topList,bars,barsloop,multibar)
						}
					}else{
						await getitem(auth,info,listname,min,topList.get(listname),loopi,userfieldMap,combineMap,renameMap,topList,bars,barsloop,multibar)
					}
				}
                
            }

            ///////////////////////////////////////////////////////////////////////////////////Clone DocLib

            await documentlib()
            let tempLib = await createlog(Dest,info,auth,DocLib,bars,multibar,barsloop,topList)

            barsloop = tempLib.barsloop
            userfieldMap = tempLib.userfieldMap
            combineMap = tempLib.combineMap


            let title = DocLib.keys()
            let directory = DocLib.values()
            for(var i = 0; i < DocLib.size; i++){
                var titleName = title.next().value
                var directoryName = directory.next().value
                //let currentitem = 1 //await itemcount(info.toTenant,info.toSite,auth.toheader,titleName)
                let top = await itemcount(info.fromTenant,info.fromSite,auth.fromheader,titleName)
                
                if(directoryName!="Shared Documents" && directoryName!="SiteAssets"){
                    await createDocLib(info,auth,titleName);
                }
                // bars.push(multibar.create(top,0,{progress:listname + " files"}));
                // barsloop++;
                if(top>1000){
                    for(let currentitem = 0; currentitem<top; (currentitem+=1000)){
                        //console.log((currentitem+1)+"   "+(currentitem+1+999))
                        errorLog += await getdocumentlib(info,auth,titleName,directoryName,(currentitem+1),errorLog,bars,barsloop);
                    }
                }else{
                    errorLog += await getdocumentlib(info,auth,titleName,directoryName,0,errorLog,bars,barsloop)
                }
                await getColumnTitle(info,auth,DocMap,titleName,await decodeName(directoryName),listID,SiteList,userfieldMap,combineMap,bars,multibar,barsloop)
            }
            if(errorLog!=''){
                fs.writeFile( 'error.txt', errorLog,'utf8', function (error: any) {
                    if (error) throw error;
                });
            }
            ///////////////////////////////////////////////////////////////////////////////////Clone DocLib


        })();

                
        async function getlistinsite(){
            auth.fromheader['Accept'] = 'application/json; odata=verbose';

            await request.get({
                url: "https://"+info.fromTenant+".sharepoint.com"+info.fromSite+"/_api/web/lists",
                headers: auth.fromheader,
                json: true
            }).then(async function(listresponse:any){
                for(var key in listresponse.d.results){
                    if(listresponse.d.results[key].BaseTemplate == 100){
                        SiteList.set(listresponse.d.results[key].Title,listresponse.d.results[key].Id)
                    }                    
                }
            })
        }

        async function documentlib(){
            auth.fromheader['Accept'] = 'application/json; odata=verbose';

            await request.get({
                url: "https://"+info.fromTenant+".sharepoint.com"+info.fromSite+"/_api/web/lists",
                headers: auth.fromheader,
                json: true
            })
            .then(async function(listresponse:any){
                for(var key in listresponse.d.results){
                    if(listresponse.d.results[key].BaseTemplate == 101){
                        //if(listresponse.d.results[key].Title=='Test'){//|| listresponse.d.results[key].Title=='doclib1'  
                            DocLib.set(listresponse.d.results[key].Title,decodeName(listresponse.d.results[key].EntityTypeName))
                            DocMap.set(listresponse.d.results[key].Title,listresponse.d.results[key].Id)
                        //}
                    }                    
                }
            })
        }
    }

    public help(){

    }
}