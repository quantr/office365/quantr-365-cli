import { QuantrCommand } from './QuantrCommand';
import * as spauth from 'node-sp-auth';
import * as request from 'request-promise';

const chalk = require('chalk');

export class CreateListCommand extends QuantrCommand {
    public color = chalk.rgb(255, 255, 0);

    public run() {
        function bodygenerate(body:any,bodydata:any){
            //console.log(bodydata)
            for(var i=0; i<bodydata.length; i++){
                //console.log("bodydata: "+ bodydata[i]);
                //body = {'FieldTypeKind': state}
                if(bodydata[i]=="type"){
                    //console.log(bodydata[i]+"____"+bodydata[i+1])
                    var bodytitle=bodydata[i+1];
                    body["FieldTypeKind"]=bodydata[i+1];
                }
                if(bodydata[i]=="title"){
                    //console.log(bodydata[i+1])
                    var bodytitle=bodydata[i+1];
                    body["Title"]=bodydata[i+1];
                }
                if(bodydata[i]=="required"){
                    //console.log("requied");
                    var requiredistrue = (bodydata[i+1]==="true")
                    //console.log(bodydata[i+1]+"____"+istrue);
                    body["Required"]=requiredistrue;
                }
                if(bodydata[i]=="default"){
                    var bodydefault= bodydata[i+1];
                    body["DefaultValue"]=bodydata[i+1];
                }
                if(bodydata[i]=="unique"){
                    var uniqueistrue = (bodydata[i+1]==="true")
                    body["Indexed"] = uniqueistrue;
                    body["EnforceUniqueValues"]=uniqueistrue;
                }
                if(bodydata[i]=="readonly"){
                    var readistrue = (bodydata[i+1]==="true")
                    body["ReadOnlyField"]=readistrue;
                }
                /*if(bodydata[i]=="format"){
                    var bodyformat = bodydata[i+1];
                    body["DisplayFormat"]=bodydata[i+1];
                }
                if(bodydata[i]=="max"){
                    //body["Max"]=bodydata[i+1];
                    body["SchemaXml"]="<Field Type=\"Number\" DisplayName=\""+bodytitle+"\" Max=\""+bodydata[i+1]+"\"/>";
                }
                if(bodydata[i]=="min"){
                    body["SchemaXml"]="<Field Type=\"Number\" DisplayName=\""+bodytitle+"\" Min=\""+bodydata[i+1]+"\"/>";
                    //body["SchemaXml"]=bodydata[i+1];
                }*/
            }
            return body;
        }

        async function addfield(fielddata:string,toUsername:string,toPassword:string,toTenant:string,toSite:string,toList:string){
            //console.log("fielddata :" + fielddata);
            await spauth.getAuth('https://' + toTenant + '.sharepoint.com', {
            username: toUsername,
            password: toPassword
        }).then(async data => {
            let headers = data.headers;
            //console.log("fielddata :" + fielddata);
            await request.post({
                url: "https://" + toTenant + ".sharepoint.com" + toSite + "/_api/contextinfo",
                headers: headers,
                rejectUnauthorized: false,
                json: true
            }).then(async response => {
                let formDigestValue = response.FormDigestValue;
                //console.log("fielddata :" + fielddata);
                //tempdata[0] must be title
                var tempvalue = fielddata.split(/=|,/);
                //var tempvalue = tempdata[0].split(/=|,/);
                //console.log("tempvalue: "+tempvalue[0]+tempvalue[1]+tempvalue[2]);
                var body = {};
                bodygenerate(body,tempvalue);
                //console.log("body_:"+body)

                var title;
                for(var i=0; i<tempvalue.length; i++){
                    if(tempvalue[i]=="title"){
                        //console.log("title: "+title);
                        title=tempvalue[i+1];
                    }
                }
                let url = "https://" + toTenant + ".sharepoint.com" + toSite + "/_api/web/lists/getbytitle('"+toList+"')/fields"; ///getbytitle('"+ this.toList+"')/fields
                let addviewurl = "https://" + toTenant + ".sharepoint.com" + toSite + "/_api/web/lists/getbytitle('"+toList+"')/views/getbytitle('All%20Items')/ViewFields/AddViewField('"+title+"')"; //('"+viewguid+"')
                //console.log("fielddata :" + fielddata);
                await request.post(
                    {
                        url: url,
                        body: body,
                        headers: {
                            "X-RequestDigest": formDigestValue,
                            "accept": "application/json; odata=verbose",
                            "content-type": "application/json",
                            "Cookie": headers.Cookie
                        },
                        rejectUnauthorized: false,
                        json: true

                }).then(async response => {
                    //console.log(response);
                    //console.log("fielddata :" + fielddata);
                    await request.post({
                        url: addviewurl,
                        headers:{
                            "X-RequestDigest": formDigestValue,
                            "accept": "application/json; odata=verbose",
                            "content-type": "application/json; odata=verbose",
                            "Cookie": headers.Cookie
                        }
                    }).then(response => {
                        return true;
                        //console.log(response);
                    })
                    return true;
                });
                return true;
            });
            return true;
        });
        return true;
        }
        
        /*function delay() {
            return new Promise((resolve, reject) => {
                    setTimeout(resolve, 500);
            });
        }*/

        super.log("Create List");
        spauth.getAuth('https://' + this.toTenant + '.sharepoint.com', {
            username: this.toUsername,
            password: this.toPassword
        }).then(data => {
            let headers = data.headers;
            request.post({
                url: "https://" + this.toTenant + ".sharepoint.com" + this.toSite + "/_api/contextinfo",
                headers: headers,
                rejectUnauthorized: false,
                json: true
            }).then(response => {
                
                let formDigestValue = response.FormDigestValue;
                //console.log(this.body);
                headers['X-RequestDigest'] = formDigestValue;
                let url = "https://" + this.toTenant + ".sharepoint.com" + this.toSite + "/_api/web/lists";
                request.post({
                    url: url,
                    headers: headers,
                    rejectUnauthorized: false,
                    body: {
                        'AllowContentTypes': true,
                        'BaseTemplate': 100,
                        'Title': this.toList,
                    },
                    json: true
                }).then(async response => {
                    //console.log(response);		

                    if(this.field!=null || this.field!=undefined){
                        
                        var infield = this.field.split("::");
                            //console.log(field[0]+"__"+field[1]+"__"+field[2]);
                        (async()=>{
                                for(let f of infield){
                                    //console.log(f);
                                    await addfield(f,this.toUsername,this.toPassword,this.toTenant,this.toSite,this.toList);
                            }
                        })();
                    }
                        
                    

                    
                });
            });
                
        });
    }

    public help() {

    }
}
