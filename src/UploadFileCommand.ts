import { QuantrCommand } from './QuantrCommand';
import * as request from 'request-promise';
import { getRandomInt } from './function';
const chalk = require('chalk');
const fs = require('fs');
const clipg = require('cli-progress')
import { uploadLargeFile } from './function';
import * as spauth from 'node-sp-auth';

export class UploadFileCommand extends QuantrCommand {
	public color = chalk.rgb(255, 255, 0);
	public file: string;

	public run() {
		super.log("Upload File");

		spauth.getAuth('https://' + this.toTenant + '.sharepoint.com', {
			username: this.toUsername,
			password: this.toPassword
		}).then(data => {
			var headers = data.headers;

			var fs = require("fs");
			var stats = fs.statSync(this.file);
			var fileSizeInBytes = stats["size"];
			//console.log(fileSizeInBytes);
			var that = this;

			// upload small file
			// fs.readFile(this.file, function (err: any, data: any) {
			// 	if (err) {
			// 		return console.log(err);
			// 	}

			// 	// if (fileSizeInBytes <= 10485760) {
			// 	request.post({
			// 		url: "https://" + that.toTenant + ".sharepoint.com/_api/contextinfo",
			// 		headers: headers,
			// 		json: true
			// 	}).then(async response => {
			// 		let formDigestValue = response.FormDigestValue;
			// 		var path = require("path");
			// 		var fileName = path.basename(that.file);

			// 		//Normal upload when file size is smaller than 10MB
			// 		var url = 'https://' + that.toTenant + '.sharepoint.com/' + that.toSite + `/_api/web/getfolderbyserverrelativeurl('` + encodeURIComponent(that.path) + `')/files/add(overwrite=true,url='` + encodeURIComponent(fileName).replace(/'/g, "''") + `')`;
			// 		headers['X-RequestDigest'] = formDigestValue;
			// 		headers['Content-Type'] = 'application/octet-stream';
			// 		request.post({
			// 			url: url,
			// 			headers: headers,
			// 			body: data
			// 		}).then(response => {
			// 			console.log(response);
			// 		});
			// 	});
			// 	// }
			// });
			// end upload small file

			// upload large file
			(async () => {
				let data
				await fs.readFile(this.file, function (err: any, d: any) {
					data = d;
				});


				let formDigestValue;
				await request.post({
					url: "https://" + that.toTenant + ".sharepoint.com/_api/contextinfo",
					headers: headers,
					json: true
				}).then(async response => {
					formDigestValue = response.FormDigestValue;
				});

				// step 1: create empty file
				let path = require("path");
				let fileName = path.basename(that.file);
				let encodedFilename = encodeURIComponent(fileName).replace(/'/g, "''");
				let url = 'https://' + that.toTenant + '.sharepoint.com/' + that.toSite + `/_api/web/getfolderbyserverrelativeurl('` + encodeURIComponent(that.path) + `')/files/add(overwrite=true,url='` + encodedFilename + `')`;
				headers['X-RequestDigest'] = formDigestValue;
				headers['Content-Type'] = 'application/octet-stream';
				await request.post({
					url: url,
					headers: headers
				}).then(response => {
					console.log('create empty file success ' + response.code);
				});

				// step 2: saveBinaryStream
				// url = 'https://' + that.toTenant + '.sharepoint.com/' + that.toSite + `/_api/web/getfolderbyserverrelativeurl('` + encodeURIComponent(that.path) + '/' + encodeURIComponent(fileName).replace(/'/g, "''") + `')/savebinarystream`;
				// headers['X-RequestDigest'] = formDigestValue;
				// headers['Content-Type'] = 'application/octet-stream';
				// await request.post({
				// 	url: url,
				// 	headers: headers
				// }).then(response => {
				// 	console.log('create empty file success ' + response.code);
				// });

				// // start upload
				let guid = '5cbbe7dc-5ed7-45e9-aab5-224349d47f39';//getRandomInt(1000, 9000);
				url = 'https://' + that.toTenant + '.sharepoint.com/' + that.toSite + `/_api/web/getfolderbyserverrelativeurl('` + encodeURIComponent(that.path) + `')/files(` + encodedFilename + `)/startUpload(uploadId=guid'` + guid + `')`;
				console.log(url);
				await request.post({
					url: url,
					headers: headers,
					body: data
				}).then(response => {
					console.log(response);
				});
			})();



			// var path = require("path");
			// var fileName = path.basename(that.file);
			// var guid = getRandomInt(1000000000, 9000000000);


			// var readStream = fs.createReadStream(this.file, { highWaterMark: 1 * 1024, encoding: 'utf8' });
			// readStream.on('data', function (chunk: any) {
			// 	// console.log('chunk Data : ')
			// 	// console.log(chunk);

			// }).on('end', function () {
			// 	console.log('###################');
			// });
			// end upload large file
		});
	}
	public help() {

	}
}