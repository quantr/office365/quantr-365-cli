import { QuantrCommand } from './QuantrCommand';
import * as spauth from 'node-sp-auth';
import * as request from 'request-promise';
import { getAuth, createall, gettop, ensureuser, getField } from './function';
const Json2csvParser = require('json2csv').Parser;
const fs = require('fs');
const chalk = require('chalk');

export class GetListCommand extends QuantrCommand {
	public color = chalk.rgb(255, 255, 0);
	
	public run() {
		super.log("Get List");

			let that = this;
			spauth.getAuth('https://' + this.fromTenant + '.sharepoint.com', {
				username: this.fromUsername,
				password: this.fromPassword
			}).then(data => {
				var headers = data.headers;
				headers['Accept'] = 'application/json;odata=verbose';				
				request.get({
					//url:"https://quantr.sharepoint.com/tiffany/tiffanytest/_api/web/lists/getByTitle('annual leave')/items(2)",
					url: "https://" + this.fromTenant + ".sharepoint.com" + this.fromSite + "/_api/web/lists/getByTitle('" + this.fromList + "')/views/getbytitle('All%20Items')/ViewFields?$select=*",
					//url: "https://" + this.fromTenant + ".sharepoint.com" + this.fromSite + "/_api/web/siteusers",
					headers: headers,
					json: true
				}).then(async function (listresponse) {
					//var responseJSON = listresponse.d.results;
					var responseJSON = listresponse.d;
					//console.log(listresponse.d.results);
					// const json2csvParser = new Json2csvParser({ header: true });
					// const json = json2csvParser.parse(JSON.parse(JSON.stringify(responseJSON)));
					fs.writeFile(that.fromList + '.json', JSON.stringify(responseJSON, null, 4), function (error: any) {
						if (error) throw error;
					});
				});
			});
	
		
		//console.log("from site is:"+this.fromSite);

		//console.log("https://" + this.fromTenant + ".sharepoint.com" + this.fromSite + "/_api/web/lists/getByTitle('" + this.fromList + "')/fields");
	}


	public help() {

	}
}