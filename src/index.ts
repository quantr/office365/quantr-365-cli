const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');
const Json2csvParser = require('json2csv').Parser;
const fs = require('fs');
const minimist = require('minimist');
const clipg = require('cli-progress');
import { CloneListCommand } from "./CloneListCommand";
import { ExportListCommand } from "./ExportListCommand";
import { CreateListCommand } from "./CreateListCommand";
import { GetListCommand } from "./GetListCommand";
import { ImportListCommand } from "./ImportListCommand";
import { CloneSiteCommand } from "./CloneSiteCommand";
import { DownloadFileCommand } from "./DownloadFileCommand";
import { UploadFileCommand } from "./UploadFileCommand";
import { DeleteListCommand } from "./DeleteListCommand"; 
import { EmptyListCommand } from "./EmptyListCommand"; 

console.log(
	chalk.rgb(255, 0, 255)(
		figlet.textSync('Quantr 365')
	)
);

var argv = minimist(process.argv.slice(2));
var command = argv.c.toLowerCase();
if (command == null || typeof command !== 'string') {
	console.log('Please specific command by -c');
	process.exit()
}

if (command == 'clonelist') {
	let temp = new CloneListCommand();

	temp.fromUsername = argv.username;
	temp.fromPassword = argv.password;
	temp.toUsername = argv.usernameDest;//can be empty
	temp.toPassword = argv.passwordDest;//can be empty

	let fromTenant = argv.from.replace('https://', '').replace(/.sharepoint.com.*/, '');
	let fromSite = argv.from.replace(/.*sharepoint.com/, '').replace(/\/Lists.*/i, ''); //subsites after tenant
	let fromList = argv.from.replace(/.*Lists./, '').replace(/\/.*/, '');
	fromList.replace(/%20/g, " ");

	let toTenant = argv.to.replace('https://', '').replace(/.sharepoint.com.*/, '');
	let toSite = argv.to.replace(/.*sharepoint.com/, '').replace(/\/Lists.*/i, '').replace(/\/Lists.*/i, '').replace(/\/_layouts.*/g, ''); //subsites after tenant
	//var toList = argv.to.replace(/.*Lists*./,'').replace(/\/.*/,'');
	//toList.replace("%20", " ");
	temp.itemindex = argv.itemindex
	temp.listindex = argv.listindex

	temp.fromTenant = fromTenant;
	temp.fromSite = fromSite;
	temp.fromList = fromList;

	temp.toTenant = toTenant;
	temp.toSite = toSite;
	temp.toList = fromList;
	/*if(toList!=undefined && toList!=""){
		temp.toList = toList;
	}else{
		temp.toList = fromList;
		//temp.toList = fromList;
	}*/
	temp.run();
}
else if (command == 'clonesite') {
	let temp = new CloneSiteCommand();

	temp.title = argv.titlefield;
	temp.fromUsername = argv.username;
	temp.fromPassword = argv.password;
	temp.toUsername = argv.usernameDest;//can be empty
	temp.toPassword = argv.passwordDest;//can be empty

	let fromTenant = argv.from.replace('https://', '').replace(/.sharepoint.com.*/, '');
	//var fromSite = argv.from.replace(/.*sharepoint.com/, '').replace(/\/Lists.*/i,''); //subsites after tenant
	let fromSite = argv.from.replace(/.*sharepoint.com/, '').replace(/\/Lists.*/i, '').replace(/\/Lists.*/i, '').replace(/\/_layouts.*/g, ''); //subsites after tenant
	let reg = new RegExp(".*" + fromSite + "/Lists/")
	let fromList = argv.from.replace(reg, '').replace(/\/.*/, '');
	//fromList.replace("%20", " ");

	let toTenant = argv.to.replace('https://', '').replace(/.sharepoint.com.*/, '');
	let toSite = argv.to.replace(/.*sharepoint.com/, '').replace(/\/Lists.*/i, '').replace(/\/Lists.*/i, '').replace(/\/_layouts.*/g, ''); //subsites after tenant
	//var toList = argv.to.replace(/.*Lists*./,'').replace(/\/.*/,'');
	//toList.replace("%20", " ");
	temp.itemindex = argv.itemindex
	temp.listindex = argv.listindex

	temp.fromTenant = fromTenant;
	temp.fromSite = fromSite;
	//temp.fromList = fromList;

	temp.toTenant = toTenant;
	temp.toSite = toSite;
	//temp.toList = fromList;
	/*if(toList!=undefined && toList!=""){
		temp.toList = toList;
	}else{
		temp.toList = fromList;
		//temp.toList = fromList;
	}*/
	temp.run();
}
else if (command == 'importlist') {
	let temp = new ImportListCommand();
	temp.filename = argv.file;
	let toList = argv.to.replace(/.*Lists\//i, '').replace(/\/.*/, '');
	let toSite = argv.to.replace(/.*sharepoint.com/, '').replace(/\/Lists.*/i, ''); //subsites after tenant
	let toTenant = argv.to.replace('https://', '').replace(/.sharepoint.com.*/, '');

	temp.toTenant = toTenant;
	temp.toSite = toSite;
	temp.toList = toList;
	temp.toUsername = argv.username;
	temp.toPassword = argv.password;
	temp.run();
}
else if (command == 'exportlist') {
	let temp = new ExportListCommand();
	temp.fromUsername = argv.username;
	temp.fromPassword = argv.password;

	let fromSite = argv.from.replace(/.*sharepoint.com/, '').replace(/\/Lists.*/i, ''); //subsites after tenant
	let fromTenant = argv.from.replace('https://', '').replace(/.sharepoint.com.*/, '');
	let fromList = argv.from.replace(/.*Lists\//i, '').replace(/\/.*/, '');
	temp.fromTenant = fromTenant;
	temp.fromSite = fromSite;
	temp.fromList = fromList;
	temp.run();
}
else if (command == 'createlist') {
	let temp = new CreateListCommand();
	temp.toUsername = argv.username;
	temp.toPassword = argv.password;
	temp.toList = argv.listname;
	//create fields
	temp.field = argv.field;

	let toSite = argv.to.replace(/.*sharepoint.com/, '').replace(/\/Lists.*/i, ''); //subsites after tenant
	let toTenant = argv.to.replace('https://', '').replace(/.sharepoint.com.*/, '');

	temp.toTenant = toTenant;
	temp.toSite = toSite;
	temp.run();
}
else if (command == 'exportlist') {
	let temp = new ExportListCommand();
	temp.fromUsername = argv.username;
	temp.fromPassword = argv.password;

	let fromSite = argv.from.replace(/.*sharepoint.com/, '').replace(/\/Lists.*/i, ''); //subsites after tenent
	let fromTenant = argv.from.replace('https://', '').replace(/.sharepoint.com.*/, '');
	let fromList = argv.from.replace(/.*Lists\//i, '').replace(/\/.*/, '');
	temp.fromTenant = fromTenant;
	temp.fromSite = fromSite;
	temp.fromList = fromList;
	temp.run();
}
else if (command == 'getlist') {
	let temp = new GetListCommand();
	temp.fromUsername = argv.username;
	temp.fromPassword = argv.password;

	let fromSite = argv.from.replace(/.*sharepoint.com/, '').replace(/\/Lists.*/i, ''); //subsites after tenent
	let fromTenant = argv.from.replace('https://', '').replace(/.sharepoint.com.*/, '');
	let fromList = argv.listname;
	temp.fromTenant = fromTenant;
	temp.fromSite = fromSite;
	temp.fromList = fromList;
	temp.run();
}
else if (command == 'downloadfile') {
	let temp = new DownloadFileCommand();
	temp.fromTenant = argv.url.replace('https://', '').replace(/.sharepoint.com.*/, '');
	temp.fromUsername = argv.username;
	temp.fromPassword = argv.password;
	temp.url = argv.url;
	temp.run();
}
else if (command == 'uploadfile') {
	let temp = new UploadFileCommand();
	temp.toUsername = argv.username;
	temp.toPassword = argv.password;
	temp.toTenant = argv.to.replace('https://', '').replace(/.sharepoint.com.*/, '');
	temp.toSite = argv.to.replace(/.*sharepoint.com/, '').replace(/\/Lists.*/i, '').replace(/\/Lists.*/i, '').replace(/\/_layouts.*/g, ''); //subsites after tenant
	temp.path = argv.path;
	temp.file = argv.file;
	temp.run();
}
else if (command == 'deletelist') {
	let temp = new DeleteListCommand();
	temp.fromUsername = argv.username;
	temp.fromPassword = argv.password;

	let fromSite = argv.from.replace(/.*sharepoint.com/, '').replace(/\/Lists.*/i, ''); //subsites after tenent
	let fromTenant = argv.from.replace('https://', '').replace(/.sharepoint.com.*/, '');
	temp.fromTenant = fromTenant;
	temp.fromSite = fromSite;
	temp.run();
}
else if (command == 'emptylist') {
	let temp = new EmptyListCommand();
	temp.fromUsername = argv.username;
	temp.fromPassword = argv.password;

	let fromSite = argv.from.replace(/.*sharepoint.com/, '').replace(/\/Lists.*/i, ''); //subsites after tenent
	let fromTenant = argv.from.replace('https://', '').replace(/.sharepoint.com.*/, '');
	let fromList = argv.from.replace(/.*Lists./, '').replace(/\/.*/, '');
	fromList.replace(/%20/g, " ");

	temp.fromTenant = fromTenant;
	temp.fromSite = fromSite;
	temp.fromList = fromList;
	temp.run();
}

