const chalk = require('chalk');
const figlet = require('figlet');

export abstract class QuantrCommand {
	protected color: any;

	public fromUsername: string;
	public toUsername: string;

	public fromPassword: string;
	public toPassword: string;

	public fromTenant: string;
	public toTenant: string;

	public fromSiteUrl: string;
	public toSiteUrl: string;

	public fromList: string;
	public toList: string;
	
	public fromSite: string;
	public toSite: string;

	public path: string;

	public title: string;

	public filename: string;

	//field item
	public state: number;
	public field: string;
	public fielddata: string;
	public body: string;
	
	public itemindex:number;
	public listindex:number;

	//DownloadFile
	public url:string;

	public log(str: string) {
		console.log(this.color(str));
	}
}