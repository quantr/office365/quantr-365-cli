import { QuantrCommand } from './QuantrCommand';
import * as spauth from 'node-sp-auth';
import * as request from 'request-promise';
import { getAuth, createall, gettop, ensureuser, deleteList } from './function';
const Json2csvParser = require('json2csv').Parser;
const fs = require('fs');
const chalk = require('chalk');

export class EmptyListCommand extends QuantrCommand {
    public color = chalk.rgb(255, 255, 0);

    public run() {
        super.log("Emplty List");
        var info = {
            fromUsername: this.fromUsername,
            fromPassword: this.fromPassword,
            fromTenant: this.fromTenant,
            fromSite: this.fromSite,
            fromList: this.fromList,
        }

        console.log('fromList='+info.fromList);

        spauth.getAuth('https://' + this.fromTenant + '.sharepoint.com', {
            username: this.fromUsername,
            password: this.fromPassword
        }).then(data => {
            var headers = data.headers;
            headers['Accept'] = 'application/json; odata=verbose';
            request.get({
                url: "https://" + this.fromTenant + ".sharepoint.com" + this.fromSite + "/_api/web/lists/getbytitle('" + info.fromList + "')/items",
                headers: headers,
                json: true
            }).then(async function (data) {
                let items = data.d.results;
                for (let n in items) {
                    request.get({
                        url: "/_api/Web/Lists/getByTitle('" + info.fromList + "')/items/getItemById(" + n + ")",
                        headers: headers,
                        json: true
                    }).then(async function (data) {

                    })
                }
            })
        })
    }

    public help() {

    }
}