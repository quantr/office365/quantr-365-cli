import { QuantrCommand } from './QuantrCommand';
import * as request from 'request-promise';
import { getAuth, createall, gettop, itemcount, createlog, clonelist, getitem } from './function';
const chalk = require('chalk');
const fs = require('fs');
const clipg = require('cli-progress')

export class CloneListCommand extends QuantrCommand {
	public color = chalk.rgb(255, 255, 0);

	public run() {
		super.log("Clone List");

		var Dest: boolean;
		var info = {
			fromUsername: this.fromUsername,
			fromPassword: this.fromPassword,
			fromTenant: this.fromTenant,
			fromSite: this.fromSite,
			fromList: this.fromList,
			toTenant: this.toTenant,
			toSite: this.toSite,
			toUsername: this.toUsername,
			toPassword: this.toPassword
		}
		//classify same tenant clone(Dest=false) || different tenant clone(Dest=true)
		if (this.toUsername != null || this.toUsername != undefined) {
			info['toUsername'] = this.toUsername;
			info['toPassword'] = this.toPassword;
			Dest = true;
		} else {
			info['toUsername'] = this.fromUsername;
			info['toPassword'] = this.fromPassword;
			Dest = false;
		}
		/*
		***Store the new ID of list for further lookup
		listMap{
			Listname => List new ID
		}

		***Store the fields with different internal name and display name
		renameMap{
			Listname => Map {
				Field InternalName => Field DisplayName
			}
		}

		***Store the fields that the in type of people and group
		userfieldMap{
			Listname => Map {
				Field InternalName => Field DisplayName
			}
		}

		***Store the siteusers ID
		combineMap{
			Source SiteUsers ID => Destination SiteUsers ID
		}

		***Store the largest item ID of the list
		toplist{
			Listname => Largest item ID of the list
		}
		 */
		var listMap = new Map();
		var renameMap = new Map();
		var userfieldMap = new Map();
		var combineMap = new Map();
		var topList = new Map();
		var auth: any; //authentication info.
		var barsLoop: number = 0; //number of bars created

		const bars: any[] = [];

		const multibar = new clipg.MultiBar({
			format: '{progress} |' + chalk.rgb(204, 0, 204)('{bar}') + '| {percentage}% || {value}/{total} Chunks',
			barCompleteChar: '\u2588',
			barIncompleteChar: '\u2591',
			hideCursor: true,
			stopOnComplete: true
		});

		(async () => {
			//get credential
			auth = await getAuth(info);
			//get the new ID of target list in listMap
			await checkfirst();
			var firstid = listMap.values().next().value;
			//get the new ID of lookup list in liatMap
			await checknext(firstid, firstid)
			listMap.delete("User Information List")
			//get the largest item ID of the list
			topList = await gettop(info, auth, listMap);

			var list = listMap.keys()
			//check if start from previous progress
			if (this.itemindex != undefined && this.listindex != undefined) {
				console.log("Continue cloning ......")
				var newlist = listMap.keys()
				for (var i = 0; i < this.listindex; i++) {
					listMap.delete(newlist.next().value)
				}
				var currentlist = list.next().value
				let currentitem = await itemcount(info.toTenant, info.toSite, auth.toheader, currentlist)
				let previousitem = await itemcount(info.fromTenant, info.fromSite, auth.fromheader, currentlist)
				bars.push(multibar.create(previousitem - currentitem, 0, { progress: currentlist + " items" }));
				barsLoop++;

				var itemloop = (topList.get(currentlist) - this.itemindex) / 1000
				for (var j = 0; j < itemloop; j++) {
					await getitem(auth, info, currentlist, (this.itemindex + (1000 * j)), ((this.itemindex + 1000 * j) + 999), this.listindex, userfieldMap, combineMap, renameMap, topList, bars, barsLoop, multibar)
					//console.log((this.itemindex+(1000*j))+"----->"+((this.itemindex+1000*j)+999))
				}
				listMap.delete(currentlist)

				for (var loopi = 0; loopi < listMap.size; loopi++) {
					var listname = list.next().value
					bars.push(multibar.create(topList.get(listname), 0, { progress: listname + " items" }));
					barsLoop++;
					if (topList.get(listname) > 1000) {
						var loop = topList.get(listname) / 1000

						for (var i = 0; i < loop; i++) {
							await getitem(auth, info, listname, ((1000 * i) + 1), (1000 * (i + 1)), loopi, userfieldMap, combineMap, renameMap, topList, bars, barsLoop, multibar)
						}
					} else {
						await getitem(auth, info, listname, 1, topList.get(listname), loopi, userfieldMap, combineMap, renameMap, topList, bars, barsLoop, multibar)
					}
				}
			}
			//check if missing input
			else if (this.itemindex == undefined && this.listindex != undefined) {
				console.error(chalk.bgRed.bold("Missing ItemIndex!"))
			}
			//check if missing input
			else if (this.itemindex != undefined && this.listindex == undefined) {
				console.error(chalk.bgRed.bold("Missing ListIndex!"))
			}
			//normal run without previous progress
			else {
				let templog = await createlog(Dest, info, auth, listMap, bars, multibar, barsLoop, topList)
				barsLoop = templog.barsloop

				userfieldMap = templog.userfieldMap
				combineMap = templog.combineMap

				var listID = await createall(info, auth, listMap)
				var min = 1; //default

				let templist = await clonelist(auth, info, listMap, listID, bars, multibar, barsLoop)
				barsLoop = templist.barsloop
				renameMap = templist.renameMap

				var list = listMap.keys()
				for (var loopi = 0; loopi < listMap.size; loopi++) {
					var listname = list.next().value
					//let item = await itemcount(info.fromTenant,info.fromSite,auth.fromheader,listname)
					bars.push(multibar.create(topList.get(listname), 0, { progress: listname + " items" }));
					barsLoop++;
					if (topList.get(listname) > 1000) {
						var loop = topList.get(listname) / 1000
						for (var i = 0; i < loop; i++) {
							await getitem(auth, info, listname, ((1000 * i) + 1), (1000 * (i + 1)), loopi, userfieldMap, combineMap, renameMap, topList, bars, barsLoop, multibar)
						}
					} else {
						await getitem(auth, info, listname, min, topList.get(listname), loopi, userfieldMap, combineMap, renameMap, topList, bars, barsLoop, multibar)
					}
				}

			}
			multibar.stop();
		})();

		//get the new ID of target list in listMap
		async function checkfirst() {
			auth.fromheader['Accept'] = 'application/json;odata=verbose';
			await request.get({
				url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getbytitle('" + info.fromList + "')",
				headers: auth.fromheader,
				json: true
			})
				.then(async function (listresponse) {
					listMap.set(info.fromList.replace(/%20/g, ' '), listresponse.d.Id)
				})
		}

		//get the new ID of lookup list in listMap
		async function checknext(firstlayer: string, currentlayer: string) {
			auth.fromheader['Accept'] = 'application/json;odata=verbose';
			var fieldresult = await request.get({
				url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists(guid'" + currentlayer + "')/fields",
				headers: auth.fromheader,
				json: true
			})
				.then(async function (listresponse) {
					return listresponse.d.results
				})

			for (var key in fieldresult) {
				if (fieldresult[key].hasOwnProperty) {
					if (fieldresult[key].FromBaseType == false && fieldresult[key].LookupList != undefined && fieldresult[key].LookupList != "") {
						var nextid = fieldresult[key].LookupList.replace(/[{}]/g, '');

						if (firstlayer != nextid) {
							await request.get({
								url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists(guid'" + nextid + "')",
								headers: auth.fromheader,
								json: true
							})
								.then(async function (listresponse) {
									listMap.set(listresponse.d.Title, nextid)
								})

							await checknext(firstlayer, nextid);
						}
					}
				}
			}
		}

	}
	public help() {

	}
}