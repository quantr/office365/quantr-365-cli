import * as spauth from 'node-sp-auth';
import * as request from 'request-promise';
const fs = require('fs');
const chalk = require('chalk');
const toStream = require('buffer-to-stream')
const stream = require("stream");
var StringDecoder = require('string_decoder').StringDecoder;

//get credentials for request
export async function getAuth(info: any) {
    var toheader = await spauth.getAuth('https://' + info.toTenant + '.sharepoint.com', {
        username: info.toUsername,
        password: info.toPassword
    })
        .then(async data => {
            return data.headers;
        })

    var fromheader = await spauth.getAuth('https://' + info.fromTenant + '.sharepoint.com', {
        username: info.fromUsername,
        password: info.fromPassword
    })
        .then(async data => {
            return data.headers;
        })

    var formDigestValue = await request.post({
        url: "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/contextinfo",
        headers: toheader,
        rejectUnauthorized: false,
        json: true
    })
        .then(async response => {
            return response.FormDigestValue;
        })

    return {
        toheader: toheader,
        fromheader: fromheader,
        formDigestValue: formDigestValue
    }
}

//add users to site
export async function ensureuser(info: any, auth: any, email: string) {
    auth.toheader['Accept'] = 'application/json; odata=verbose';
    auth.toheader['X-RequestDigest'] = auth.formDigestValue;
    return request.post({
        url: "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/ensureuser",
        headers: auth.toheader,
        body: {
            'logonName': 'i:0#.f|membership|' + email
        },
        json: true
    })
        .then(async function (listresponse) {
            return listresponse.d.Id;
        })
        .catch(async function (response) {
            return email
        })
}

//create all list
export async function createall(info: any, auth: any, list: any) {
    var listID = new Map();
    var listname = list.keys()
    var old = list.values()
    let url = "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists";
    auth.toheader['Accept'] = 'application/json; odata=verbose';
    auth.toheader['X-RequestDigest'] = auth.formDigestValue;
    for (var i = 0; i < list.size; i++) {
        var title = listname.next().value
        var oldid = old.next().value
        await request.post({
            url: url,
            headers: auth.toheader,
            rejectUnauthorized: false,
            body: {
                'AllowContentTypes': true,
                'BaseTemplate': 100,
                'Title': title
            },
            json: true
        })
            .then(async next => {
                var newid = next.d.Id;
                listID.set(oldid, newid)
            })
    }
    return listID;
}

//get the number of items in list
export async function itemcount(tenant: string, site: string, header: any, listname: string) {
    header['Accept'] = 'application/json;odata=verbose';
    return await request.get({
        url: "https://" + tenant + ".sharepoint.com" + site + "/_api/web/lists/getbytitle('" + listname + "')/ItemCount",
        headers: header,
        json: true
    })
        .then(async function (listresponse) {
            return listresponse.d.ItemCount;
        })
}

//get the largest item ID of the list
export async function gettop(info: any, auth: any, list: any) {
    var savetop = list.keys();
    var topList = new Map();
    for (var i = 0; i < list.size; i++) {
        var toplistname = savetop.next().value;

        auth.fromheader['Accept'] = 'application/json;odata=verbose';

        var top = await itemcount(info.fromTenant, info.fromSite, auth.fromheader, toplistname)

        if (top == 0) {
            topList.set(toplistname, 0)
        }
        else {
            await request.get({
                url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getbytitle('" + toplistname + "')/items?$orderby=ID desc&$top=1",
                headers: auth.fromheader,
                json: true
            }).then(async function (listresponse) {
                if (listresponse.d.results[0].ID > top) {
                    top = listresponse.d.results[0].ID
                }
            })
            topList.set(toplistname, top)
        }
    }
    return topList;
}

//create a log to show the users that are not in Active Directory
export async function createlog(Dest: boolean, info: any, auth: any, listMap: any, bars: any[], multibar: any, barsloop: number, topList: any) {
    var userfieldMap = new Map();
    var useremailMap = new Map();
    var destemailMap = new Map();
    var combineMap = new Map();

    auth.toheader['Accept'] = 'application/json;odata=verbose';
    auth.fromheader['Accept'] = 'application/json;odata=verbose';
    var arrayData: any[][] = [];
    var arrayHeader = ["Created", "ItemID", "FieldName", "Email"]

    var list = listMap.keys()
    console.log("Creating siteusers log ......")

    //get info. of destination site's siteuser
    await request.get({
        url: "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/siteusers",
        headers: auth.toheader,
        json: true
    })
        .then(function (listresponse) {
            for (var key in listresponse.d.results) {
                if (listresponse.d.results[key].hasOwnProperty) {
                    destemailMap.set(JSON.parse(JSON.stringify(listresponse.d.results[key].Email).toLowerCase()), listresponse.d.results[key].Id)
                }
            }
        })

    //filter all exist users in both site
    await request.get({
        url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/siteusers",
        headers: auth.fromheader,
        json: true
    })
        .then(function (listresponse) {
            for (var key in listresponse.d.results) {
                if (listresponse.d.results[key].hasOwnProperty) {
                    useremailMap.set(listresponse.d.results[key].Id, JSON.parse(JSON.stringify(listresponse.d.results[key].Email).toLowerCase()))

                }
            }
        })
        .then(function (next) {
            var oldMap = useremailMap.values();
            var oldMapkey = useremailMap.keys();

            for (var i = 0; i < useremailMap.size; i++) {
                var oldid = oldMapkey.next().value;
                var oldvalue = oldMap.next().value;
                if (destemailMap.has(oldvalue) && oldvalue != "") {
                    //new siteuser lists after filtering
                    combineMap.set(oldid, destemailMap.get(oldvalue))
                }

            }
        })

    for (var loopi = 0; loopi < listMap.size; loopi++) {
        var listname = list.next().value
        bars.push(multibar.create(1, 0, { progress: "Checking " + listname }));
        barsloop++;

        //get people and group fields in each list
        await request.get({
            url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getbytitle('" + listname + "')/fields", //?$select=Endorser/Title,Endorser/EMail&$top=200&$expand=Endorser
            headers: auth.fromheader,
            json: true
        })
            .then(function (listresponse) {
                let tempobj = new Map()
                for (var key in listresponse.d.results) {
                    if (listresponse.d.results[key].hasOwnProperty) {
                        if (listresponse.d.results[key].FieldTypeKind == 20) {
                            tempobj.set(listresponse.d.results[key].InternalName, listresponse.d.results[key].Title)
                        }
                    }
                }
                userfieldMap.set(listname, tempobj)
            })

        //create csv file to store the non-exist people and group in destination tenant to current directory
        if (topList.get(listname) > 5000) {
            var loop = topList.get(listname) / 5000

            for (var i = 0; i < loop; i++) {
                await request.get({
                    url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getbytitle('" + listname + "')/items?$top=5000&$filter=(ID ge " + ((5000 * i) + 1) + ") and (ID le " + (5000 * (i + 1)) + ")",
                    headers: auth.fromheader,
                    json: true
                }).then(async function (listresponse) {
                    await getlogdata(Dest, info, auth, listname, listresponse, arrayData, userfieldMap, useremailMap, combineMap);
                })
            }
        } else {
            await request.get({
                url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getbytitle('" + listname + "')/items?$top=5000",
                headers: auth.fromheader,
                json: true
            }).then(async function (listresponse) {
                //console.log(listresponse)
                await getlogdata(Dest, info, auth, listname, listresponse, arrayData, userfieldMap, useremailMap, combineMap);
            })
        }
        //console.log(arrayData)
        if (arrayData != undefined && arrayData.length > 0) {
            let header = arrayHeader.join(",") + '\n';
            let csvhead = header;
            arrayData.forEach(arr => {
                csvhead += arr.join(",") + '\n';
            });
            fs.writeFileSync(listname + '.csv', csvhead);
            arrayData = []
        }
        bars[barsloop - 1].increment();
    }
    return {
        userfieldMap: userfieldMap,
        combineMap: combineMap,
        barsloop: barsloop
    }

}

//get key in map by value
async function getkey(map: any, searchvalue: string) {
    var temp = map.keys()
    for (var i = 0; i < map.size; i++) {
        var keyvalue = temp.next().value
        if (map.get(keyvalue) == searchvalue) {
            return keyvalue
        }
    }
}


//save the generated error log data into arrayData
async function getlogdata(Dest: boolean, info: any, auth: any, listname: string, listresponse: any, arrayData: any, userfieldMap: any, useremailMap: any, combineMap: any) {
    var currentdate = new Date();
    var datetime = currentdate.getFullYear() + "/" + (currentdate.getMonth() + 1) + "/" + currentdate.getDate() + " " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
    var fields = userfieldMap.get(listname).keys()
    var name = userfieldMap.get(listname).values()
    for (var i = 0; i < userfieldMap.get(listname).size; i++) {
        var fieldname = fields.next().value
        var title = name.next().value
        for (var key in listresponse.d.results) {
            if (listresponse.d.results[key].hasOwnProperty) {
                //console.log(listresponse.d.results[key])
                if (listresponse.d.results[key][fieldname + "Id"] != null) {
                    if (typeof listresponse.d.results[key][fieldname + "Id"] == "object") {
                        let array = listresponse.d.results[key][fieldname + "Id"].results
                        for (var k in array) {
                            if (array[k].hasOwnProperty) {
                                if (combineMap.has(array[k]) == false) {
                                    if (useremailMap.get(array[k]) != undefined && useremailMap.get(array[k]) != "") {
                                        if (Dest == false) {
                                            let oldid = await getkey(useremailMap, useremailMap.get(array[k]))
                                            let newid = await ensureuser(info, auth, useremailMap.get(array[k]))
                                            if (typeof newid == 'number') {
                                                combineMap.set(oldid, newid)
                                            } else {
                                                arrayData.push([datetime, listresponse.d.results[key].ID, title, "(Allow multiple selections)" + useremailMap.get(array[k])])
                                            }
                                        } else {
                                            arrayData.push([datetime, listresponse.d.results[key].ID, title, "(Allow multiple selections)" + useremailMap.get(array[k])])
                                        }
                                    }
                                    //console.log("Created: "+datetime+"		ItemID: "+listresponse.d.results[key].ID + "		FieldName: " + title +"		Email (Allow multiple selections): "+useremailMap.get(array[k]))							
                                }
                            }
                        }
                    }

                    else {
                        if (combineMap.has(listresponse.d.results[key][fieldname + "Id"]) == false) {
                            if (useremailMap.get(listresponse.d.results[key][fieldname + "Id"]) != undefined && useremailMap.get(listresponse.d.results[key][fieldname + "Id"]) != "") {
                                if (Dest == false) {
                                    let oldid = await getkey(useremailMap, useremailMap.get(listresponse.d.results[key][fieldname + "Id"]))
                                    let newid = await ensureuser(info, auth, useremailMap.get(listresponse.d.results[key][fieldname + "Id"]))

                                    if (typeof newid == 'number') {
                                        combineMap.set(oldid, newid)
                                    } else {
                                        arrayData.push([datetime, listresponse.d.results[key].ID, title, useremailMap.get(listresponse.d.results[key][fieldname + "Id"])])
                                    }
                                } else {
                                    arrayData.push([datetime, listresponse.d.results[key].ID, title, useremailMap.get(listresponse.d.results[key][fieldname + "Id"])])
                                }
                            }
                            //console.log("Created: "+datetime+"		ItemID: "+listresponse.d.results[key].ID + "		FieldName: " + title +"		Email: "+useremailMap.get(listresponse.d.results[key][fieldname+"Id"]))
                        }
                    }

                }
                //console.log(listresponse.d.results[key][fieldname+"Id"])
            }
        }

    }
}

export async function getView(auth: any, info: any, listname: string) { //Get StaticName of viewFields 
    let viewDisplayName = await request.get({ //arr
        url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getByTitle('" + listname + "')/views?$filter=DefaultView eq true",
        headers: auth.fromheader,
        json: true
    }).then(async function (listresponse) {
        console.log(listresponse.d.results[0].Title);
        return listresponse.d.results[0].Title;
    });
    return await request.get({ //arr
        url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getByTitle('" + listname + "')/views/getbytitle('" + encodeURIComponent(viewDisplayName) + "')/ViewFields", //StaticName
        headers: auth.fromheader,
        json: true
    }).then(async function (listresponse) {
        return listresponse.d.Items.results;
    });
}

//clone a individual list
export async function clonelist(auth: any, info: any, map: any, ID: any, bars: any, multibar: any, barsloop: number) {
    var renameMap = new Map();
    var list = map.keys()
    var source = map.values()
    console.log("Cloning lists ......")
    for (var i = 0; i < map.size; i++) {
        var listname = list.next().value
        var sourceid = source.next().value
        auth.fromheader['Accept'] = 'application/json;odata=verbose';

        var viewfields = await getView(auth, info, listname);
        console.log(viewfields);
        var fields = await request.get({
            url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getByTitle('" + listname + "')/fields",
            headers: auth.fromheader,
            json: true
        })
            .then(async function (listresponse) {
                return listresponse.d.results;
            })

        var allFields = await getField(auth, info, listname); //EntityPropertyNmae
        //bars.push(multibar.create(Object.keys(allFields).length, 0, { progress: listname + " fields" }));
        bars.push(multibar.create(viewfields.length, 0, { progress: listname + " fields" }));
        barsloop++;

        /* 
        1.check 2 items result ,delete default fields
        2.get the rest fields(EntityPropertyName),compare if they are same with internal name
        3.if no, use internal name
        4.if yes, use the list of field name as list to add field
        5.display name might diff
        */

        for (var n in allFields) {
            for (var key in fields) {
                if (fields.hasOwnProperty(key)) {
                    //var trimname = fields[key].InternalName.replace(/_x003a_/g, ":").replace(/_x0020_/g, " ");
                    var trimname = decodeName(fields[key].InternalName);
                    //Got the fields in view
                    if (n == fields[key].InternalName) {
                        //copy all field data into xml
                        var xml = fields[key].SchemaXml.replace(/SourceID=".+?"/g, "SourceID=\"" + sourceid + "\" EntityPropertyName=\"" + fields[key].EntityPropertyName + "\" InternalName=\"" + fields[key].InternalName + "\"") //correct source id in xml
                        var reflistid = xml.match(/List="{(.*?)}/)
                        //check lookup field & replace the old lookup id
                        if (reflistid != undefined && reflistid != "" && reflistid != null) {
                            var newid = ID.get(reflistid[1])
                            xml = xml.replace(/List=".+?"/g, "List=\"" + newid + "\"")
                        }
                        //Update the title name if display name ≠ internal name 
                        if (fields[key].InternalName == n && fields[key].InternalName == "LinkTitle") {
                            var bodytitle = { "Title": fields[key].Title }
                            await updatelist(info, auth, listname, "Title", bodytitle); //rename if title hv another name

                        }
                        //insert normal field
                        else if (fields[key].Title == fields[key].InternalName && fields[key].FromBaseType == false) {
                            await insertfieldxml(info, auth, listname, fields[key].InternalName, xml, false)
                        }
                        //insert edited field
                        else if (fields[key].Title != fields[key].InternalName && fields[key].FromBaseType == false) {
                            let tempMap = new Map();
                            if (fields[key].Title != trimname) {
                                tempMap.set(fields[key].InternalName, fields[key].Title)
                                renameMap.set(listname, tempMap)
                                xml = xml.replace(/DisplayName=".+?"/g, "DisplayName=\"" + fields[key].InternalName + "\"")
                                await insertfieldxml(info, auth, listname, fields[key].InternalName, xml, false)
                                await updatelist(info, auth, listname, fields[key].InternalName, { "Title": fields[key].Title })
                            } else {
                                await insertfieldxml(info, auth, listname, fields[key].InternalName, xml, false)
                            }
                        }
                        //insert default field
                        else if (fields[key].StaticName == n && fields[key].FromBaseType == true) {
                            var bodydefault = { "ReadOnlyField": false }
                            await updatelist(info, auth, listname, fields[key].Title, bodydefault);
                            //console.log(listresponse.d.results[key].InternalName)
                            await insertListView(info, auth, listname, fields[key].Title, false)
                        }
                    }
                }
            }
            bars[barsloop - 1].increment();
        }
    }
    return {
        renameMap: renameMap,
        barsloop: barsloop
    }
}

//update a individual list
export async function updatelist(info: any, auth: any, listname: string, old: string, updatebody: any) {
    let update = "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists/getbytitle('" + listname + "')/fields/getbytitle('" + old + "')"; ///getbytitle('"+ this.toList+"')/fields
    await request.post({
        url: update,
        body: updatebody,
        headers: {
            "X-RequestDigest": auth.formDigestValue,
            "accept": "application/json; odata=verbose",
            "content-type": "application/json",
            "Cookie": auth.toheader.Cookie,
            "If-Match": "{etag or *}",
            "X-HTTP-Method": "MERGE"
        },
        rejectUnauthorized: false,
        json: true
    })
        .then(async response => {
        })
}

//insert field by xml data
async function insertfieldxml(info: any, auth: any, listname: string, title: string, xml: any, docLib: boolean) {
    let url = "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists/getbytitle('" + listname + "')/fields/createFieldAsXml"
    await request.post({
        url: url,
        headers: {
            "X-RequestDigest": auth.formDigestValue,
            "accept": "application/json; odata=verbose",
            "content-type": "application/json; odata=verbose",
            "Cookie": auth.toheader.Cookie,
            //"If-Match": "{etag or *}",
            //"X-HTTP-Method": "MERGE"
        },
        body: {
            "parameters": {
                "__metadata": { "type": "SP.XmlSchemaFieldCreationInformation" },
                "SchemaXml": xml,
            }
        },
        rejectUnauthorized: false,
        json: true
    })
        .then(async response => {
            await insertListView(info, auth, listname, title, docLib)
        })
        .catch(async function (err) {
            console.error(chalk.bgRed.bold("InsertFieldXml Error"));
            process.exit();
        })

}

//set the new added field as visible in ListView
async function insertListView(info: any, auth: any, listname: string, title: string, docLib: boolean) {
    let addviewurl
    if (docLib === false) {
        addviewurl = "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists/getbytitle('" + listname + "')/views/getbytitle('All%20Items')/ViewFields/AddViewField('" + title + "')"; //('"+viewguid+"')
    }
    else if (docLib === true) {
        addviewurl = "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists/getbytitle('" + listname + "')/views/getbytitle('All%20Documents')/ViewFields/AddViewField('" + title + "')"; //('"+viewguid+"')
    }
    await request.post({
        url: addviewurl,
        headers: {
            "X-RequestDigest": auth.formDigestValue,
            "accept": "application/json; odata=verbose",
            "content-type": "application/json; odata=verbose",
            "Cookie": auth.toheader.Cookie
        }
    })
}

//get field in view
export async function getField(auth: any, info: any, listname: string) {
    auth.fromheader['Accept'] = 'application/json;odata=nometadata';
    return await request.get({
        url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getByTitle('" + listname + "')/items?$top=1",
        headers: auth.fromheader,
        json: true
    })
        .then(async function (listresponse) {
            var deleteItem = ['FileSystemObjectType', 'Id', 'ID', 'ServerRedirectedEmbedUri', 'ServerRedirectedEmbedUrl', 'ContentTypeId', 'ComplianceAssetId', 'Modified', 'Created', 'AuthorId', 'EditorId', 'OData__UIVersionString', 'Attachments', 'GUID'];
            deleteItem.forEach(e => delete listresponse[e]);
            return listresponse;
        })
}

//get all the item in list
export async function getitem(auth: any, info: any, listname: string, lowerlimit: number, upperlimit: number, loopindex: number, userfieldMap: any, combineMap: any, renameMap: any, topList: any, bars: any, barsloop: number, multibar: any) {//userfieldMap:any,combineMap:any
    var isarrayMap = new Map();
    auth.fromheader['Accept'] = 'application/json;odata=nometadata';

    var listitem = await request.get({
        url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getByTitle('" + listname + "')/items?$top=5000&$filter=(ID ge " + lowerlimit + ") and (ID le " + upperlimit + ")",
        headers: auth.fromheader,
        json: true
    })
        .then(async function (listresponse) {
            return listresponse;
        })

    var inarr = listitem.value[listitem.value.length - 1].ID;
    while (inarr-- > lowerlimit) {
        if (!listitem.value.some((item: any) => item.ID == inarr)) {
            var insertindex = listitem.value.findIndex((x: any) => x.ID == (inarr + 1))
            listitem.value.splice(insertindex, 0, { ID: inarr, Id: inarr, Title: 'TempEmptyData' });
        }
    }
    for (var key in listitem.value) {
        if (listitem.value.hasOwnProperty(key)) {
            for (var k in listitem.value[key]) {
                if (listitem.value[key].hasOwnProperty(k)) {
                    var value = listitem.value[key][k];
                    if (Array.isArray(value)) {
                        isarrayMap.set(k, k);
                    }
                }
            }
        }
    }
    for (var key in listitem.value) {
        if (listitem.value.hasOwnProperty(key)) {
            //if(Dest==true){
            for (var k in listitem.value[key]) {
                if (listitem.value[key].hasOwnProperty(k)) {
                    var value = listitem.value[key][k];
                    var index = k.slice(0, -2)

                    if (isarrayMap.size != 0) {
                        if (value == null && k == isarrayMap.get(k)) {
                            delete listitem.value[key][k];
                            delete listitem.value[key][index + 'StringId']
                        }
                    }
                    if (userfieldMap.get(listname) != undefined && userfieldMap.get(listname).has(index)) {
                        if (combineMap.get(listitem.value[key][k]) != undefined) {
                            listitem.value[key][k] = combineMap.get(listitem.value[key][k])
                            if (k != "AuthorId" && k != "EditorId") {
                                listitem.value[key][index + 'StringId'] = listitem.value[key][k].toString()
                            }
                        } else if (typeof listitem.value[key][k] == "object" && listitem.value[key][k] != null) {
                            var result = [];
                            for (var i in listitem.value[key][k]) {
                                if (combineMap.has(listitem.value[key][k][i])) {
                                    result.push(combineMap.get(listitem.value[key][k][i]))
                                }
                            }
                            if (result.length == 0) {
                                delete listitem.value[key][k];
                                delete listitem.value[key][index + 'StringId']
                            } else {
                                listitem.value[key][k] = result;
                                listitem.value[key][index + 'StringId'] = result.map(String)
                            }
                        } else {
                            delete listitem.value[key][k];
                            delete listitem.value[key][index + 'StringId']
                        }
                    }

                }
            }
            /*}else{
                console.log("no dest")
                for(var k in listitem.value[key]) {
                    if(listitem.value[key].hasOwnProperty(k)) {
                        var value = listitem.value[key][k];
                        if(value == null && k == isarrayMap.get(k)){
                            delete listitem.value[key][k];
                        }
                    }
                }

            }*/

            await insertitem(auth, info, listname, listitem.value[key], loopindex, renameMap, topList, bars, barsloop);
            if (listitem.value[key].Attachments == true) {
                await attach(auth, info, listname, listitem.value[key].ID);
            }
        }

    }
    await deleteitem(info, auth, listname, listitem.value, bars, barsloop)
}

//delete a temp data item
async function deleteitem(info: any, auth: any, listname: string, bodydata: any, bars: any, barsloop: number) {
    for (var key = 0; key <= bodydata.length; key++) {
        if (bodydata.hasOwnProperty(key)) {
            if (bodydata[key].Title == 'TempEmptyData') {
                var url = "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists/getbytitle('" + listname + "')/items(" + bodydata[key].ID + ")"
                await request.post({
                    url: url,
                    headers: {
                        "X-RequestDigest": auth.formDigestValue,
                        "accept": "application/json; odata=verbose;",
                        "Cookie": auth.toheader.Cookie,
                        "If-Match": "*",
                        "X-HTTP-Method": "DELETE"
                    },
                    rejectUnauthorized: false,
                })
                    .then(async result => {
                        //bars[barsloop-1].increment();
                    })

            }

        }
    }
}

//insert a single item
async function insertitem(auth: any, info: any, listname: string, bodydata: any, loopindex: number, renameMap: any, topList: any, bars: any, barsloop: number) {
    var lastupdate: string;
    let tempauth = await getAuth(info)
    if (renameMap.size != 0) {
        for (var key in bodydata) {
            if (bodydata.hasOwnProperty(key)) {
                var index = key.slice(0, -2);
                if (renameMap.get(listname) != undefined && renameMap.get(listname).get(index) != undefined) {
                    //console.log(renameMap.get(listname).get(index))
                    var newkey = renameMap.get(listname).get(index)
                    bodydata[newkey + "Id"] = bodydata[key]
                    delete bodydata[key];
                }
            }
        }
    }
    //delete bodydata.ID
    //delete bodydata.Id
    let url = "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists/getbytitle('" + listname + "')/items";
    await request.post({
        url: url,
        body: bodydata,
        headers: {
            "X-RequestDigest": tempauth.formDigestValue,
            "accept": "application/json; odata=verbose",
            "content-type": "application/json",
            "Cookie": tempauth.toheader.Cookie,
        },
        rejectUnauthorized: false,
        json: true
    })
        .then(async result => {
            if (result.d.ID == topList.get(listname)) {
                lastupdate = "0";
            } else {
                lastupdate = result.d.ID
            }
            bars[barsloop - 1].increment();
        })
        .catch(async function (err) {
            console.error(chalk.bgRed.bold("Unexpected error in List: " + listname + " is found, this program terminated in ItemIndex: " + lastupdate + ". Please start again with ListIndex: " + loopindex + " , ItemIndex: " + (lastupdate + 1) + "."));
            process.exit();
        })
}

//update a single item
async function updateitem(auth: any, info: any, listname: string, bodydata: any) {
    let url = "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists/getbytitle('" + listname + "')/items(" + bodydata.ID + ")";
    delete bodydata.ID
    delete bodydata.Id
    await request.post({
        url: url,
        body: bodydata,
        headers: {
            "X-RequestDigest": auth.formDigestValue,
            "accept": "application/json; odata=verbose",
            "content-type": "application/json",
            "Cookie": auth.toheader.Cookie,
            "If-Match": "*",
            "X-HTTP-Method": "MERGE"
        },
        rejectUnauthorized: false,
        json: true
    })

}

//loop through every attachments within a single item
async function attach(auth: any, info: any, listname: string, ID: number) {
    auth.fromheader['Accept'] = 'application/json;odata=verbose';
    var attachfile = await request.get({
        url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getbytitle('" + listname + "')/items(" + ID + ")/AttachmentFiles",
        headers: auth.fromheader,
        json: true
    })
        .then(async function (listresponse) {
            for (var key in listresponse.d.results) {
                if (listresponse.d.results[key].hasOwnProperty) {
                    await getattach(auth, info, listname, ID, listresponse.d.results[key].FileName);
                }
            }
        })
        .catch(async function (response) {
            //errorcode:505 connection error
            //console.log("attach: ")
            console.log(response)
        })


}

//get attachment in list item
async function getattach(auth: any, info: any, listname: string, ID: number, filename: string) {
    auth.fromheader['binaryStringResponseBody'] = "true";

    await request.get({
        url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getbytitle('" + listname + "')/items(" + ID + ")/AttachmentFiles/getbyfilename('" + encodeURIComponent(filename) + "')/$value",
        headers: auth.fromheader,
        encoding: null
    })
        .then(async function (listresponse) {
            await copyattach(auth, info, listname, filename, ID, listresponse);
            //console.log(listresponse);
        })
        .catch(async function (response) {
            //console.log("getattach: ")
            console.log(response)
        })

}

//copy attachment to destination list
async function copyattach(auth: any, info: any, listname: string, filename: string, ID: number, filedata: any) {
    var url = "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists/getbytitle('" + listname + "')/items(" + ID + ")/AttachmentFiles/add(FileName='" + encodeURIComponent(filename) + "')"; //" + filename + "

    await request.post({
        url: url,
        body: filedata,
        headers: {
            "X-RequestDigest": auth.formDigestValue,
            "Content-Type": undefined,
            "accept": "application/json; odata=verbose;",
            "binaryStringRequestBody": "true",
            "Cookie": auth.toheader.Cookie,
        },
        rejectUnauthorized: false,
    })
        .then(async result => {

        })
        .catch(async function (response) {
            //console.log("copyattach: ")
            console.log(response)
        })

}

function timer(ms: number) {
    return new Promise(res => setTimeout(res, ms));
}

//create a empty DOcument Library
export async function createDocLib(info: any, auth: any, title: string) {
    await request.post({
        url: "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists",
        body: {
            'BaseTemplate': 101,
            'Title': title
        },
        headers: {
            "Accept": "application/json; odata=verbose",
            "X-RequestDigest": auth.formDigestValue,
            "Cookie": auth.toheader.Cookie
        },
        json: true
    })
}

//get the Document library data 
export async function getdocumentlib(info: any, auth: any, title: string, directory: string, currentitem: number, errorLog: string, bars: any, barsloop: number) {
    auth = await getAuth(info);
    auth.fromheader['Accept'] = 'application/json; odata=nometadata';
    var topLevel = info.fromSite + "/" + directory
    var bodydata = await request.get({
        url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getbytitle('" + title + "')/items?$select=File,Folder&$top=1000&$filter=(ID ge " + currentitem + ") and (ID le " + (currentitem + 999) + ")&$expand=File,Folder,File/ListItemAllFields,Folder/ListItemAllFields", //&$filter=(ID ge "+currentitem+") and (ID le "+(currentitem+4999)+")
        //url: "https://"+info.fromTenant+".sharepoint.com"+info.fromSite+"/_api/web/getfolderbyserverrelativeurl('Shared Documents/arduino')?$expand=Files,Files/ListItemAllFields",
        headers: auth.fromheader,
        json: true
    }).then(async function (listresponse: any) {
        var tempResult;
        for (var key in listresponse.value) {
            //console.log(listresponse.value[key])
            if (listresponse.value[key].Folder != undefined) {
                let folderLevel = listresponse.value[key].Folder.ServerRelativeUrl.substr(0, listresponse.value[key].Folder.ServerRelativeUrl.lastIndexOf('/'))
                if (topLevel.toLowerCase() == folderLevel.toLowerCase()) {

                    tempResult = await copyFolder(info, auth, listresponse.value[key].Folder.ServerRelativeUrl.replace(/\//, '').toLowerCase(), title, directory.toLowerCase(), bars, barsloop);
                    if (tempResult != null) {
                        errorLog = errorLog + tempResult + '\n'
                    }
                }
            } else if (listresponse.value[key].File != undefined) {
                let fileLevel = listresponse.value[key].File.ServerRelativeUrl.substr(0, listresponse.value[key].File.ServerRelativeUrl.lastIndexOf('/'))
                if (topLevel.toLowerCase() == fileLevel.toLowerCase()) {
                    tempResult = await copyFile(info, auth, listresponse.value[key].File.ServerRelativeUrl.replace(/\//, '').toLowerCase(), title, directory.toLowerCase(), bars, barsloop);
                    if (tempResult != null) {
                        errorLog = errorLog + tempResult + '\n'
                    }
                }
            }
        }
    })
    return errorLog;
}

//copy folder and its contents to destination 
export async function copyFolder(info: any, auth: any, relativeUrl: string, title: string, dir: string, bars: any, barsloop: number) {
    //auth = await getAuth(info);
    //console.log(relativeUrl+"   "+dir)
    await timer(1000);
    let destDirectory = relativeUrl.substr(relativeUrl.indexOf(dir))
    var postData = {
        "srcUrl": encodeURIComponent("https://" + info.fromTenant + ".sharepoint.com" + "/" + relativeUrl),
        "destUrl": encodeURIComponent("https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/" + destDirectory)
    }
    //console.log("!!!!!!srcurl"+postData.srcUrl+"\n"+"!!!!!!desturl"+postData.destUrl)

    let err = await request.post({
        url: "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/SP.MoveCopyUtil.CopyFolder",//CopyFolderByPath()",
        body: postData,
        headers: {
            "Accept": "application/json; odata=verbose",
            "X-RequestDigest": auth.formDigestValue,
            "Cookie": auth.toheader.Cookie
        },
        rejectUnauthorized: false,
        json: true
    })
        .then(async function (response: any) {
            //bars[barsloop-1].increment();
            return null;
        })
        .catch(async function (err: any) {
            console.log("err:" + err + "\n" + destDirectory)
            return destDirectory;
        })
    return err;
}


//copy file contents to destination 
export async function copyFile(info: any, auth: any, relativeUrl: string, title: string, dir: string, bars: any, barsloop: number) {
    //auth = await getAuth(info);
    await timer(200)
    //console.log("copyfile: "+relativeUrl+"  "+dir)

    let destDirectory = relativeUrl.substr(relativeUrl.indexOf(dir))
    var postData = {
        "srcUrl": encodeURIComponent("https://" + info.fromTenant + ".sharepoint.com" + "/" + relativeUrl), //encodeURIComponent
        "destUrl": encodeURIComponent("https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/" + destDirectory)
    }
    //console.log("!!!!!!srcurl"+postData.srcUrl+"\n"+"!!!!!!desturl"+postData.destUrl)
    //console.log("File: "+relativeUrl)

    let err = await request.post({
        url: "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/SP.MoveCopyUtil.CopyFile",//CopyFolderByPath()",
        body: postData,
        headers: {
            "Accept": "application/json; odata=verbose",
            "X-RequestDigest": auth.formDigestValue,
            "Cookie": auth.toheader.Cookie
        },
        rejectUnauthorized: false,
        json: true
    })
        .then(async function (response: any) {
            //bars[barsloop-1].increment();
            return null;
        })
        .catch(async function (err: any) {
            //console.log("err:"+err+"\n"+destDirectory)
            return destDirectory;
        })
    return err;

}

//get the Column Name of the document library(if have column other than title)
export async function getColumnTitle(info: any, auth: any, DocMap: any, listName: string, directory: string, listID: any, map: any, userfieldMap: any, combineMap: any, bars: any, multibar: any, barsloop: number) {
    let fromheader = auth.fromheader
    let lookupColumn = new Map();
    fromheader['Accept'] = 'application/json;odata=nometadata';
    console.log("Cloning Document Library ----- " + listName)
    var viewfields = await request.get({ //arr
        url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getByTitle('" + listName + "')/views/getbytitle('All%20Documents')/ViewFields",///getbytitle('All%20Items')/ViewFields
        headers: fromheader,
        json: true
    })
        .then(async function (listresponse) {
            //console.log(listresponse)
            return listresponse.Items;
        })
    //console.log(viewfields)
    //return viewfields;
    var fields = await request.get({
        url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getByTitle('" + listName + "')/fields",
        headers: fromheader,
        json: true
    })
        .then(async function (listresponse) {
            return listresponse.value;
        })
    // bars.push(multibar.create(viewfields.length,0,{progress: listName+" fields"}));
    // barsloop++;

    for (let i = viewfields.length - 1; i >= 0; i--) {
        if (viewfields[i] == 'DocIcon' || viewfields[i] == 'LinkFilename') {
            viewfields.splice(i, 1);
        }
    }

    for (let k in viewfields) {
        for (let key in fields) {
            if (viewfields[k] == fields[key].InternalName) {
                var trimname = decodeName(fields[key].InternalName);
                //console.log(fields[key].Title +"    "+fields[key].InternalName+"    "+trimname)
                //check whether the DisplayName is equal InternalName 

                var xml = fields[key].SchemaXml.replace(/SourceID=".+?"/g, "SourceID=\"{" + DocMap.get(listName) + "}\"") //correct source id(oldid) in xml

                var reflistid = xml.match(/List="{(.*?)}/)
                //check lookup field & replace the old lookup id
                if (reflistid != undefined && reflistid != "" && reflistid != null) {
                    lookupColumn.set(fields[key].InternalName, fields[key].InternalName + "Id")
                    var newid = listID.get(reflistid[1]) //{oldid:newid}
                    xml = xml.replace(/List=".+?"/g, "List=\"{" + newid + "}\"")
                }

                if (fields[key].StaticName == viewfields[k] && fields[key].FromBaseType == true) { //editor+modified
                    //console.log(fields[key].StaticName+"    "+viewfields[k])
                    var bodydefault = { "ReadOnlyField": false }
                    await updatelist(info, auth, listName, fields[key].Title, bodydefault);
                    //await insertListView(info,auth,listName,fields[key].Title,false)
                }
                else if (fields[key].Title != trimname) {
                    xml = fields[key].SchemaXml.replace(/DisplayName=".+?"/g, "DisplayName=\"" + fields[key].InternalName + "\"")
                    await insertfieldxml(info, auth, listName, viewfields[k], xml, true)
                    await updatelist(info, auth, listName, fields[key].InternalName, { "Title": fields[key].Title })
                }
                else {
                    await insertfieldxml(info, auth, listName, viewfields[k], xml, true)
                }

                if (lookupColumn.get(fields[key].InternalName) != undefined) {
                    await getColumn(auth, info, listName, directory, lookupColumn.get(fields[key].InternalName), userfieldMap, combineMap)
                }
                else (
                    await getColumn(auth, info, listName, directory, fields[key].InternalName, userfieldMap, combineMap)
                )
            }
        }
        // bars[barsloop-1].increment();
    }

}

//get column contents 
export async function getColumn(auth: any, info: any, listName: string, directory: string, columnName: string, userfieldMap: any, combineMap: any) {

    let isarrayMap = new Map();
    let oldColumn = new Map();
    let oldItemName = new Map();
    let newColumn = new Map();

    if (columnName == 'Author' || columnName == 'Editor') {
        columnName += "Id"
    }
    await request.get({
        url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getbytitle('" + listName + "')/items?$filter= (" + columnName + " ne null)",//$filter=FileLeafRef eq 'arduino'",//CopyFolderByPath()",
        headers: auth.fromheader,
        json: true
    })
        .then(async function (response: any) {

            for (let key in response.value) {
                oldColumn.set(response.value[key].ID, response.value[key][columnName])
                //console.log(response.value[key].ID+"    "+response.value[key].test)
            }
        })
        .catch(async function (err: any) {
            console.log("error: " + err)
        })
    let oldColumnID = oldColumn.keys()
    for (let i = 0; i < oldColumn.size; i++) {
        let ID = oldColumnID.next().value
        await request.get({
            url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/lists/getbytitle('" + listName + "')/items?$filter= " + columnName + " eq '" + oldColumn.get(ID) + "' &$select=File,Folder&$expand=File,Folder,File/ListItemAllFields,Folder/ListItemAllFields",
            headers: auth.fromheader,
            json: true
        })
            .then(async function (response: any) {
                // console.log(columnName+"    "+oldColumn.get(ID))
                // console.log(response.value)
                for (let key in response.value) {
                    if (response.value[key].Folder != undefined) {
                        //console.log(response.value[key].Folder.Name+"   "+response.value[key].Folder.ListItemAllFields.ID)
                        oldItemName.set(response.value[key].Folder.ListItemAllFields.ID, response.value[key].Folder.Name)
                    } else if (response.value[key].File != undefined) {
                        //console.log(response.value[key].File.Name+" "+response.value[key].File.ListItemAllFields.ID)
                        oldItemName.set(response.value[key].File.ListItemAllFields.ID, response.value[key].File.Name)
                    }
                }
            })
            .catch(async function (err: any) {
                console.log("error: " + err)
            })

        auth.toheader["accept"] = "application/json; odata=nometadata"
        var listitem = await request.get({
            url: "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists/getbytitle('" + listName + "')/items?$filter= FileLeafRef eq '" + oldItemName.get(ID) + "'",
            headers: auth.toheader,
            json: true
        })
            .then(async function (response: any) {
                return response;

            })
        auth.toheader["accept"] = "application/json; odata=verbose"

        for (var key in listitem.value) {
            if (listitem.value.hasOwnProperty(key)) {
                for (var k in listitem.value[key]) {
                    if (listitem.value[key].hasOwnProperty(k)) {
                        var value = listitem.value[key][k];
                        if (Array.isArray(value)) {
                            isarrayMap.set(k, k);
                        }
                    }
                }
            }
        }
        for (var key in listitem.value) {
            if (listitem.value.hasOwnProperty(key)) {
                //if(Dest==true){
                for (var k in listitem.value[key]) {
                    if (listitem.value[key].hasOwnProperty(k)) {
                        var value = listitem.value[key][k];
                        var index = k.slice(0, -2)

                        if (isarrayMap.size != 0) {
                            if (value == null && k == isarrayMap.get(k)) {
                                delete listitem.value[key][k];
                                delete listitem.value[key][index + 'StringId']
                            }
                        }

                        if (userfieldMap.get(listName) != undefined && userfieldMap.get(listName).has(index)) {
                            console.log("inhere: " + listName + "   " + userfieldMap.get(listName) + "  " + index)
                            if (combineMap.get(listitem.value[key][k]) != undefined) {
                                listitem.value[key][k] = combineMap.get(listitem.value[key][k])
                                if (k != "AuthorId" && k != "EditorId") {
                                    listitem.value[key][index + 'StringId'] = listitem.value[key][k].toString()
                                }
                            } else if (typeof listitem.value[key][k] == "object" && listitem.value[key][k] != null) {
                                var result = [];
                                for (var i in listitem.value[key][k]) {
                                    if (combineMap.has(listitem.value[key][k][i])) {
                                        result.push(combineMap.get(listitem.value[key][k][i]))
                                    }
                                }
                                if (result.length == 0) {
                                    delete listitem.value[key][k];
                                    delete listitem.value[key][index + 'StringId']
                                } else {
                                    listitem.value[key][k] = result;
                                    listitem.value[key][index + 'StringId'] = result.map(String)
                                }
                            } else {
                                delete listitem.value[key][k];
                                delete listitem.value[key][index + 'StringId']
                            }
                        }

                    }
                }
                await updateColumn(auth, info, columnName, oldColumn.get(ID), listName, directory, listitem.value[key].ID)
            }
        }

    }

}

//update column contents
export async function updateColumn(auth: any, info: any, columnName: string, content: any, listname: string, directory: string, itemID: number) {
    console.log("updatecolumn: " + columnName + "    " + content)
    //let update = "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists/getbytitle('"+listname+"')/items?$filter Name eq 'arduino'"; ///getbytitle('"+ this.toList+"')/fields
    /*if(columnName=='AuthorId' || columnName=='EditorId'){
        columnName=columnName.slice(0,-2)
    }*/
    await request.post({
        url: "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists/getbytitle('" + listname + "')/items(" + itemID + ")",//CopyFolderByPath()",
        body: { __metadata: { "type": "SP.Data." + directory + "Item" }, [columnName]: content },
        // body: {
        //     [columnName]: content,
        // },
        headers: {
            "X-RequestDigest": auth.formDigestValue,
            "accept": "application/json; odata=verbose",
            "content-type": "application/json; odata=verbose",
            "Cookie": auth.toheader.Cookie,
            "If-Match": "*",
            "X-HTTP-Method": "MERGE"
        },
        rejectUnauthorized: false,
        json: true
    })
        .then(async response => {

        })
        .catch(async err => {
            console.log("error" + err)
        })
}

//upload large file(still testing)
export async function uploadLargeFile(info: any, auth: any, listName: string, fileName: string) {
    ////let guid = await guid_genearator(info,auth,listName)
    //configure package.json to allow larger memory, 5GB now
    var GUID = await createFileURL(info, auth, listName, fileName, '')
    var slice_size = 500 * 1024//8 * 1024 * 1024
    var percent_done
    var offset: number

    var decoder = new StringDecoder('utf8');
    var buffer = Buffer.alloc(slice_size), filePath = './' + fileName
    var stats = fs.statSync(filePath)
    var fileSizeInBytes = stats["size"]
    var sizeDone = 0

    await fs.open(filePath, 'r', async function (err: any, fd: any) {
        if (err) throw err;
        async function readNextChunk() {
            await fs.read(fd, buffer, 0, slice_size, null, async function (err: any, nread: number) {
                if (err) throw err;
                if (nread === 0) {
                    // done reading file, do any necessary finalization steps
                    fs.close(fd, function (err: any) {
                        if (err) throw err;
                    });
                    return;
                }

                var data: Buffer;
                //console.log(nread+" "+slice_size)
                if (sizeDone == 0) { //first buffer
                    //console.log("first buffer: ")
                    if (nread < slice_size) {
                        data = buffer.slice(0, nread);
                        //console.log("one buffer only")
                    }
                    else {

                        data = buffer;
                        //console.log("nread>=slice")
                        let Chunk = decoder.write(data);
                        offset = await startUpload(info, auth, GUID, "", fileName, data)
                    }
                    sizeDone += nread;
                    percent_done = Math.floor((sizeDone / fileSizeInBytes) * 100);
                    //console.log(percent_done+" %    "+ sizeDone)
                    await readNextChunk();

                } else {
                    if (nread < slice_size) {

                        data = buffer.slice(0, nread)
                        //console.log("last buffer")
                        let Chunk = decoder.write(data);

                        await finishUpload(info, auth, GUID, "", fileName, data, offset)

                    }
                    else {
                        if ((sizeDone + nread) == fileSizeInBytes) {
                            data = buffer
                            let Chunk = decoder.write(data);

                            await finishUpload(info, auth, GUID, "", fileName, data, offset)
                            //console.log("samesize but last buffer")
                        }
                        data = buffer
                        let Chunk = decoder.write(data);

                        offset = await continueUpload(info, auth, GUID, "", fileName, data, offset)
                        //console.log("continue....")
                    }
                    sizeDone += nread
                    percent_done = Math.floor((sizeDone / fileSizeInBytes) * 100);
                    //console.log(percent_done+" %    "+sizeDone)
                    await readNextChunk();

                }
            });
        }
        await readNextChunk();
    });
}



/*function bufferToStream(buffer:any) {
    let stream = new Readable({
        read(){
            this.push(buffer);
            //this.push(null);
        }
    });
    return stream;
}*/

//large file upload method
async function startUpload(info: any, auth: any, GUID: string, relativeUrl: string, fileName: string, chunk: any) {
    return await request.post({
        //url: "https://"+info.toTenant+".sharepoint.com"+info.toSite+"/_api/web/getfilebyserverrelativeurl('"+encodeURIComponent('Test/a.txt')+"')/startupload(uploadId='"+GUID+"', stream="+chunk+")", 
        url: "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/getFolderByServerRelativeUrl('/tiffany/clonesite/Test')/files('" + fileName + "')/startUpload(uploadId=guid'" + GUID + "')",
        body: await convertDataBinaryString(chunk),
        headers: {
            "X-RequestDigest": auth.formDigestValue,
            "accept": "application/json; odata=verbose",
            "Cookie": auth.toheader.Cookie,
            "Content-Type": 'application/octet-stream',
            //"binaryStringRequestBody": true,

        },
        rejectUnauthorized: false,
        json: true
    })
        .then(async function (response) {
            return response.d.StartUpload;
        })
}

//large file upload method
async function continueUpload(info: any, auth: any, GUID: string, relativeUrl: string, fileName: string, chunk: any, offset: number) {
    return await request.post({
        url: "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/getFolderByServerRelativeUrl('/tiffany/clonesite/Test')/files('" + fileName + "')/continueupload(uploadId=guid'" + GUID + "', fileOffset=" + offset + ")",
        body: await convertDataBinaryString(chunk),
        headers: {
            "X-RequestDigest": auth.formDigestValue,
            "accept": "application/json; odata=verbose",
            "Cookie": auth.toheader.Cookie,
            "Content-Type": 'application/octet-stream',
            //"binaryStringRequestBody": true,

        },
        rejectUnauthorized: false,
        json: true
    })
        .then(async function (response) {
            return response.d.ContinueUpload
        })
}

//large file upload method
async function finishUpload(info: any, auth: any, GUID: string, relativeUrl: string, fileName: string, chunk: any, offset: number) {
    await request.post({
        url: "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/getFolderByServerRelativeUrl('/tiffany/clonesite/Test')/files('" + fileName + "')/finishupload(uploadId=guid'" + GUID + "', fileOffset=" + offset + ")",
        body: await convertDataBinaryString(chunk),
        headers: {
            "X-RequestDigest": auth.formDigestValue,
            "accept": "application/json; odata=verbose",
            "Cookie": auth.toheader.Cookie,
            "Content-Type": 'application/octet-stream',
            //"binaryStringRequestBody": true,
        },
        rejectUnauthorized: false,
        json: true
    })
        .then(async function (response) {
            //console.log(response)
        })
}

export async function readStream(filename: string) {
    var readStream = fs.createReadStream(filename);

    // This will wait until we know the readable stream is actually valid before piping
    readStream.on('open', function () {
        // This just pipes the read stream to the response object (which goes to the client)
    });

    // This catches any errors that happen while creating the readable stream (usually invalid names)
    readStream.on('end', function () {

    });
}

//get size of a single file
export async function getFileSize(info: any, auth: any, relativeUrl: string) {
    return await request.get({
        url: "https://" + info.fromTenant + ".sharepoint.com" + info.fromSite + "/_api/web/getfilebyserverrelativeurl('" + encodeURIComponent('/Shared Documents/test iso/en_sharepoint_server_2019_x64_dvd_68e34c9e.iso') + "')",//?$expand=Files,Files/ListItemAllFields
        headers: auth.toheader,
        json: true
    })
        .then(async function (response: any) {
            return response.Length
        })
}

//add file by url
export async function createFileURL(info: any, auth: any, listName: string, fileName: string, body: any) {
    let url = "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/Lists/getByTitle('" + listName + "')/RootFolder/files/add(url='" + fileName + "',overwrite=true)"
    return await request.post({
        url: url,
        //body:body,
        headers: {
            "X-RequestDigest": auth.formDigestValue,
            "accept": "application/json; odata=verbose",
            "Cookie": auth.toheader.Cookie,
        },
        json: true
    })
        .then(async function (response: any) {
            //console.log(response.d)
            return response.d.UniqueId
        })
}

export async function decodeName(name: any) {
    const data: any = {
        '007e': '~',
        '0021': '!',
        '0040': '@',
        '0023': '#',
        '0024': '$',
        '0025': '%',
        '005e': '^',
        '0026': '&',
        '002a': '*',
        '0028': '(',
        '0029': ')',
        '002b': '+',
        '002d': '-',
        '003d': '=',
        '007b': '{',
        '007d': '}',
        '003a': ':',
        '0022': '"',
        '007c': '|',
        '003b': ';',
        '0027': '\'',
        '005c': '\\',
        '003c': '<',
        '003e': '>',
        '003f': '?',
        '002c': ',',
        '002e': '.',
        '002f': '/',
        '0060': '`',
        '0020': ' '
    }

    const result = name.replace(/_x(\d+)_/g, (match: any, key: string | number) => data[key] || match);
    return result;
}

export async function guid_genearator(info: any, auth: any, listName: string) {
    var d = Date.now();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
        d += performance.now(); //use high-precision timer if available
    }
    var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    if (await guid_exist(info, auth, listName, guid) == false) {
        return guid;
    }
    else {
        await guid_genearator(info, auth, listName)
    }
}

export function getRandomInt(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
}

//delete a single list by name
export async function deleteList(info: any, auth: any, listName: string) {
    await request.post({
        url: "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists/GetByTitle('" + listName + "')",
        headers: {
            "accept": "application/json; odata=verbose",
            "content-type": "application/json; odata=verbose",
            "X-RequestDigest": auth.formDigestValue,
            "X-HTTP-Method": "DELETE",
            //"Cookie": auth.toheader.Cookie,
            "IF-MATCH": "*"
        },
        json: true
    }).then(async function (response: any) {

    }).catch(async function (err) {
        console.error(chalk.bgRed.bold("Unexpected error in Delete List: " + listName + "."));
        console.error(err);
        process.exit();
    })




}

//check if guid exist
async function guid_exist(info: any, auth: any, listName: string, guid: string) {
    let exist = true
    await request.get({
        url: "https://" + info.toTenant + ".sharepoint.com" + info.toSite + "/_api/web/lists/getbytitle('" + listName + "')/items?$filter=UniqueId eq '" + guid + "'",
        headers: auth.toheader,
        json: true
    }).then(async function (listresponse) {
        if (listresponse.value[0] == undefined) {
            exist = false; //guid not exist
        }
        else {
            exist = true; //guid already exist
        }
        return exist;
    })
    return exist;
}

//this method slices the blob array buffer to the appropriate chunk and then calls off to get the BinaryString of that chunk  
async function convertFileToBlobChunks(result: any, byteOffset: number) {
    let arrayBuffer = result.slice();
    debugger;
    return this.convertDataBinaryString(arrayBuffer);
}

async function convertDataBinaryString(data: any) {
    let fileData = '';
    let byteArray = new Uint8Array(data);
    for (var i = 0; i < byteArray.byteLength; i++) {
        fileData += String.fromCharCode(byteArray[i]);
    }

    //console.log(fileData)
    return fileData;
}



















