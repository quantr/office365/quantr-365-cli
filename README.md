# This command not live yet, we are just designing this cli library

# Introduction

We need a set of command to manipulate the data on Office 365, especially SharePoint.

# Developers

Peter <peter@quantr.hk> , System Architect

Tiffany <tiffany@quantr.hk> , Programmer

# Installation

```
npm i -g @quantr/quantr-365-cli
```

# Development

## Compile

Edit src/index.ts, then

```
npm run build
```

## Run

```
node dist/index.js -c clonelist --username dev@quantr.hk --password xxx --from https://quantr.sharepoint.com/quantr-365-cli/lists/list1 --to https://quantr.sharepoint.com/quantr-365-cli/lists/list2
```

# Commands

This command support these functions:

1. Create list (done)
2. Copy list items to
3. Clone list (done)
4. Clone user groups
5. Export list to excel (done)
6. Import excel to list
7. Clone site 
8. Google drive to SharePoint folder

## Create empty list option

# Options
1. --username
2. --password
3. --from
4. --listname 
```
quantr-365-cli -c createlist --username peter@quantr.hk --password xxx --from https://quantr.sharepoint.com/tiffany/Lists/testing4 --listname testing4
```

## Create list

# --type(s)
1. 9 (number)
2. 2 (text)
3. 3 (note)
4. 4 (datetime)

# Propertie(s)
1. title (necessary)
2. required
3. default
4. unique
5. readonly

```
quantr-365-cli -c createlist --username peter@quantr.hk --password xxx --to https://quantr.sharepoint.com/tiffany --listname testing --field type=9,title=testnum1|type=2,title=testnum2
```

## Export list

```
quantr-365-cli -c exportlist --username peter@quantr.hk --password xxx --from https://quantr.sharepoint.com/tiffany/Lists/testing4 
```

## Clone list (Same tenant)

```
quantr-365-cli -c clonelist --username peter@quantr.hk --password xxx --from https://quantr.sharepoint.com/tiffany/Lists/claim%20form --to https://quantr.sharepoint.com/tiffany/Lists (--listindex --itemindex)
```

## Clone list (Different tenant)

```
quantr-365-cli -c clonelist --username peter@quantr.hk --password xxx --usernameDest peter@quantr.hk --passwordDest xxx --from https://quantr.sharepoint.com/dev/lists/list1 --to https://xxx.sharepoint.com/eform/lists (--listindex --itemindex)
```

## Clone Site
```
quantr-365-cli -c clonesite --username peter@quantr.hk --password xxx --from https://quantr.sharepoint.com/tiffany/Lists/claim%20form --to https://quantr.sharepoint.com/tiffany/Lists (--listindex --itemindex)
```

Problem: 
StatusCodeError: 500 - {"error":{"code":"-2130575312, Microsoft.SharePoint.SPException","message":{"lang":"en-US","value":"The URL 'fileName' is invalid.  It may refer to a nonexistent file or folder, or refer to a valid file or folder that is not in the current Web."}}}

Solution:
To work around this issue, remove the Version column from the list of Indexed Columns for the list that has this issue. To do this, follow these steps:

    1.  Browse to the list where the issue exists.

    2.  In the ribbon, click the Library tab, and then click Library Settings.

    3.  In the fields list, click Indexed Columns.

    4.  In the Indexed Columns list, click Version.

    5.  Click Delete, and then click OK.

    6.  Upload any files that you previously experienced the issue with.

## Get list

```
node dist/index.js -c getlist --username peter@quantr.hk --password xxx --from https://quantr.sharepoint.com/tiffany --listname testing6
```

## Download file

```
node dist/index.js -c downloadfile --username peter@quantr.hk --password xxx --url https://quantr.sharepoint.com/Shared%20Documents/test%20iso/en_sharepoint_server_2019_x64_dvd_68e34c9e.iso
```

## Upload file

```
node dist/index.js -c uploadfile --username peter@quantr.hk --password xxx --to https://quantr.sharepoint.com/test --path "doclib1\folder1\香港 沙田" --file "c:\users\peter\desktop\Screenshot_8.png"  
```

# Publish to npmjs.org

```
npm --access public publish
```
